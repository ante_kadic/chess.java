/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Events;

import java.util.EventListener;

/**
 *
 * @author ante.kadic
 */
public interface ChatListener extends EventListener {

    void onRecievedMessage(String message, String nickName);

}
