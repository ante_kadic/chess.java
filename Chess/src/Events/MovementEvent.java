/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Events;

import PieceModel.Piece;
import java.util.EventObject;

/**
 *
 * @author ante.kadic
 */
public class MovementEvent extends EventObject {
    
    public MovementEvent(Piece source,MovementEventArgs args) {
        super(source);
    }
    
}
