/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Timer;

/**
 *
 * @author ante.kadic
 */
public class TimerThread extends Thread {

    private int minutes;
    
    public TimerThread(int minutes) {
        this.minutes = minutes;
    }

    @Override
    public void run() {
        while(!this.isInterrupted()){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(TimerThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            
        }
    }


}
