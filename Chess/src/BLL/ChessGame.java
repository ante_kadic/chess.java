package BLL;

import Enums.*;
import Events.CapturedEvent;
import Events.CapturedListener;
import Helpers.CustomMessage;
import PieceModel.King;
import PieceModel.Piece;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ChessGame implements Serializable {

    private ArrayList<Move> moveHistory;
    private PieceColor playerTurn;
    private Result result;
    private CheckState checkState;
    private ChessBoard board;

    public transient int INIT_TIME_PLAY = 20;
    public int whiteMinutes = INIT_TIME_PLAY;
    public int blackMinutes = INIT_TIME_PLAY;
    public int whiteSeconds = 59;
    public int blackSeconds = 59;
    public CustomMessage lblMessage;

    private transient List<CapturedListener> capturedListeners;

    public ChessGame(Player player, CapturedListener listener) {
        board = new ChessBoard();
        moveHistory = new ArrayList<>();
        capturedListeners = new LinkedList<CapturedListener>();
        capturedListeners.add(listener);
        subscribeForCapturedEvent();

        playerTurn = PieceColor.White;
        checkState = CheckState.None;
        result = Result.None;
    }

    public void removeListeners(CapturedListener listener) {
        capturedListeners.remove(listener);
    }

    public void addListeners(CapturedListener listener) {
        if (capturedListeners == null) {
            capturedListeners = new LinkedList<CapturedListener>();
        }
        capturedListeners.add(listener);
    }

    public boolean makeMove(Piece piece, Square destination) {
        Move move = new Move(piece.getPosition(), destination, piece);

        boolean success = getBoard().makeMove(move);
        if (success) {
            this.setPlayerTurn(getPlayerTurn().equals(PieceColor.White) ? PieceColor.Black : PieceColor.White);
            getMoveHistory().add(move);
            if (moveRepeatedThreeTimes()) {
                raiseOnStateChanged(getPlayerTurn(), CheckState.Remi);
            }
        }
        return success;
    }

    public void subscribeForCapturedEvent() {
        getBoard().addOnCaptureListener(new CapturedListener() {
            @Override
            public void onCapturedEvent(CapturedEvent ev) {
                for (CapturedListener listener : capturedListeners) {
                    listener.onCapturedEvent(ev);
                }
            }

            @Override
            public void onCheckStateChangedEvent(PieceColor playerOnCheckState, CheckState state) {
                raiseOnStateChanged(playerOnCheckState, state);
            }
        });
    }

    public void raiseOnStateChanged(PieceColor playerOnCheckState, CheckState state) {
        setCheckState(state);
        if (state == CheckState.Checkmate) {
            setResult(playerOnCheckState == PieceColor.Black ? Result.WhiteWins : Result.BlackWins);
        } else if (state == CheckState.Remi) {
            setResult(Result.Draw);
        } else if (state == CheckState.Timeout) {
            setResult(playerOnCheckState == PieceColor.Black ? Result.WhiteWins : Result.BlackWins);
        }
        for (CapturedListener listener : capturedListeners) {
            listener.onCheckStateChangedEvent(playerOnCheckState, state);
        }
    }

    private boolean moveRepeatedThreeTimes() {
        if (getMoveHistory().size() < 6) {
            return false;
        }
        int lastIndex = getMoveHistory().size() - 1;
        Move m1 = getMoveHistory().get(lastIndex);
        Move m2 = getMoveHistory().get(lastIndex - 1);
        Move m3 = getMoveHistory().get(lastIndex - 2);
        Move m4 = getMoveHistory().get(lastIndex - 3);
        Move m5 = getMoveHistory().get(lastIndex - 4);
        Move m6 = getMoveHistory().get(lastIndex - 5);

        if (m1.equals(m5) && m2.equals(m6)) {

            if (m3.getPiece().equals(m1.getPiece()) && m2.getPiece().equals(m4.getPiece())) {
                if (m3.getDestinationSquare().equals(m1.getStartSquare()) && m3.getStartSquare().equals(m1.getDestinationSquare())) {
                    if (m2.getDestinationSquare().equals(m4.getStartSquare()) && m4.getDestinationSquare().equals(m2.getStartSquare())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @return the moveHistory
     */
    public ArrayList<Move> getMoveHistory() {
        return moveHistory;
    }

    public void setMoveHistory(ArrayList<Move> moveHistory) {
        this.moveHistory = moveHistory;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public void setCheckState(CheckState checkState) {
        this.checkState = checkState;
    }

    public Result getResult() {
        return result;
    }

    public CheckState getCheckState() {
        return checkState;
    }

    /**
     * @return the board
     */
    public ChessBoard getBoard() {
        return board;
    }

    /**
     * @return the isCurrentPlayerTurn
     */
    public PieceColor getPlayerTurn() {
        return playerTurn;
    }

    /**
     * @param isCurrentPlayerTurn the isCurrentPlayerTurn to set
     */
    public void setPlayerTurn(PieceColor turn) {
        this.playerTurn = turn;
    }
}
