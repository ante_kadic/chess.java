/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helpers;

import BLL.Move;
import BLL.Square;
import PieceModel.Piece;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author ante.kadic
 */
public class PieceImageManager {

    private static ImageFactory factory = new ImageFactory();

    public static ImageIcon getImage(Piece piece) {
        return new ImageIcon(factory.getPieceImage(piece));
    }

    public static void removeImage(Square startSquare) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
