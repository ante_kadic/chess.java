/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helpers;

import BLL.ChessBoard;
import PieceModel.Knight;
import PieceModel.Piece;
import BLL.Square;
import Enums.MovementDirection;
import Enums.PieceColor;
import PieceModel.Bishop;
import PieceModel.King;
import PieceModel.Pawn;
import PieceModel.Queen;
import PieceModel.Rook;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author ante.kadic
 */
public class PieceMovementManager {

    private static boolean isCovered;

    public static List<Square> getValidDiagonals(Square startingSquare, Piece piece) {

        LinkedList<Square> squares = new LinkedList<Square>();

        addNorthEastSquare(startingSquare, piece, squares);
        addNorthWestSquare(startingSquare, piece, squares);
        addSouthEastSquare(startingSquare, piece, squares);
        addSouthWestSquare(startingSquare, piece, squares);

        return squares;
    }

    public static List<Square> getValidAntiDiagonals(Square startingSquare, Piece piece) {

        LinkedList<Square> squares = new LinkedList<Square>();

        addNorthSquare(startingSquare, piece, squares);
        addWestSquare(startingSquare, piece, squares);
        addEastSquare(startingSquare, piece, squares);
        addSouthSquare(startingSquare, piece, squares);

        return squares;
    }

    public static List<Square> getKnightValidMoves(Square startingSquare, Knight piece) {
        LinkedList<Square> squares = new LinkedList<Square>();
        List<Square> possibleSquares = new LinkedList<>();

        possibleSquares.add(new Square(startingSquare.getX() + 2, startingSquare.getY() + 1));
        possibleSquares.add(new Square(startingSquare.getX() + 1, startingSquare.getY() + 2));
        possibleSquares.add(new Square(startingSquare.getX() - 2, startingSquare.getY() + 1));
        possibleSquares.add(new Square(startingSquare.getX() - 1, startingSquare.getY() + 2));
        possibleSquares.add(new Square(startingSquare.getX() - 2, startingSquare.getY() - 1));
        possibleSquares.add(new Square(startingSquare.getX() + 2, startingSquare.getY() - 1));
        possibleSquares.add(new Square(startingSquare.getX() - 1, startingSquare.getY() - 2));
        possibleSquares.add(new Square(startingSquare.getX() + 1, startingSquare.getY() - 2));

        for (Square square : possibleSquares) {
            if ((piece.getBoard().isEmptySquare(square) || piece.canCaptureOtherOnSquare(square)) && square.isValid()) {
                squares.add(square);
            }
        }
        return squares;
    }

    public static List<Square> getPawnValidMoves(Pawn pawn) {
        List<Square> validSquares = new LinkedList<>();

        if (pawn.getDirection() == MovementDirection.Up) {
            Square square = new Square(pawn.getPosition().getX(), pawn.getPosition().getY() + 1);
            if (pawn.getBoard().isEmptySquare(square) && square.isValid()) {
                validSquares.add(square);
            }
            if (!pawn.isIsMoved()) {
                Square sq = new Square(square.getX(), square.getY() + 1);
                if (pawn.getBoard().isEmptySquare(sq) && sq.isValid()) {
                    validSquares.add(sq);
                }
            }
        } else {
            Square square = new Square(pawn.getPosition().getX(), pawn.getPosition().getY() - 1);
            if (pawn.getBoard().isEmptySquare(square) && square.isValid()) {
                validSquares.add(square);
            }
            if (!pawn.isIsMoved()) {
                Square sq = new Square(square.getX(), square.getY() - 1);
                if (pawn.getBoard().isEmptySquare(sq) && sq.isValid()) {
                    validSquares.add(sq);
                }
            }
        }
        validSquares.addAll(getPawnNonEmptyAttackingMoves(pawn));
        return validSquares;
    }

    public static List<Square> getPawnEmptyAttackingMoves(Pawn pawn) {
        List<Square> attackingSquares = new LinkedList<>();

        if (pawn.getDirection() == MovementDirection.Up) {
            attackingSquares.add(new Square(pawn.getPosition().getX() + 1, pawn.getPosition().getY() + 1));
            attackingSquares.add(new Square(pawn.getPosition().getX() - 1, pawn.getPosition().getY() + 1));
        } else {
            attackingSquares.add(new Square(pawn.getPosition().getX() + 1, pawn.getPosition().getY() - 1));
            attackingSquares.add(new Square(pawn.getPosition().getX() - 1, pawn.getPosition().getY() - 1));
        }
        return attackingSquares;
    }

    public static List<Square> getPawnNonEmptyAttackingMoves(Pawn pawn) {
        List<Square> atackingSquares = new LinkedList<>();

        if (pawn.getDirection() == MovementDirection.Up) {
            Square square = new Square(pawn.getPosition().getX() + 1, pawn.getPosition().getY() + 1);

            boolean canAttackSquare = pawn.canCaptureOtherOnSquare(square);
            if (canAttackSquare && square.isValid()) {
                atackingSquares.add(square);
            }
            Square squ = new Square(pawn.getPosition().getX() - 1, pawn.getPosition().getY() + 1);
            boolean canAttackSqu = pawn.canCaptureOtherOnSquare(squ);
            if (canAttackSqu && squ.isValid()) {
                atackingSquares.add(squ);
            }

        } else {
            Square square = new Square(pawn.getPosition().getX() + 1, pawn.getPosition().getY() - 1);
            boolean canAttackSquare = pawn.canCaptureOtherOnSquare(square);
            if (canAttackSquare && square.isValid()) {
                atackingSquares.add(square);
            }
            Square squ = new Square(pawn.getPosition().getX() - 1, pawn.getPosition().getY() - 1);
            boolean canAttackSqu = pawn.canCaptureOtherOnSquare(squ);
            if (canAttackSqu && squ.isValid()) {
                atackingSquares.add(squ);
            }
        }
        return atackingSquares;
    }

    public static List<Square> getKingSurroundingSquares(King king) {
        List<Square> surroundingSquares = new LinkedList<>();

        surroundingSquares.add(new Square(king.getPosition().getX(), king.getPosition().getY() + 1));
        surroundingSquares.add(new Square(king.getPosition().getX() + 1, king.getPosition().getY() + 1));
        surroundingSquares.add(new Square(king.getPosition().getX() + 1, king.getPosition().getY()));
        surroundingSquares.add(new Square(king.getPosition().getX() + 1, king.getPosition().getY() - 1));
        surroundingSquares.add(new Square(king.getPosition().getX(), king.getPosition().getY() - 1));
        surroundingSquares.add(new Square(king.getPosition().getX() - 1, king.getPosition().getY() - 1));
        surroundingSquares.add(new Square(king.getPosition().getX() - 1, king.getPosition().getY()));
        surroundingSquares.add(new Square(king.getPosition().getX() - 1, king.getPosition().getY() + 1));
        return surroundingSquares;
    }

    public static List<Square> getKingValidMoves(King king) {
        List<Square> validSquares = new LinkedList<>();
        List<Square> surroundingSquares = getKingSurroundingSquares(king);

        for (Square square : surroundingSquares) {
            if ((king.getBoard().isEmptySquare(square) || king.canCaptureOtherOnSquare(square)) && square.isValid()) {
                PieceColor clr = king.getColor() == PieceColor.Black ? PieceColor.White : PieceColor.Black;
                if (!king.getBoard().isAttackedSquare(square, clr)) {
                    validSquares.add(square);
                }
            }
        }

        if (!king.isIsMoved()) {
            //TODO: implement rochade
        }

        return validSquares;
    }

    private static void addNorthWestSquare(Square startingSquare, Piece piece, LinkedList<Square> squares) {
        Square northWest = new Square(startingSquare.getX() - 1, startingSquare.getY() + 1);
        boolean canCapture = piece.canCaptureOtherOnSquare(northWest);

        if ((piece.getBoard().isEmptySquare(northWest) || canCapture) && northWest.isValid()) {
            squares.add(northWest);
            if (canCapture) {
                return;
            }
            addNorthWestSquare(northWest, piece, squares);
        }
        return;
    }

    private static void addNorthEastSquare(Square startingSquare, Piece piece, LinkedList<Square> squares) {
        Square northEast = new Square(startingSquare.getX() + 1, startingSquare.getY() + 1);
        boolean canCapture = piece.canCaptureOtherOnSquare(northEast);
        if ((piece.getBoard().isEmptySquare(northEast) || canCapture) && northEast.isValid()) {
            squares.add(northEast);
            if (canCapture) {
                return;
            }
            addNorthEastSquare(northEast, piece, squares);
        }
        return;
    }

    private static void addSouthWestSquare(Square startingSquare, Piece piece, LinkedList<Square> squares) {
        Square southWest = new Square(startingSquare.getX() - 1, startingSquare.getY() - 1);
        boolean canCapture = piece.canCaptureOtherOnSquare(southWest);
        if ((piece.getBoard().isEmptySquare(southWest) || canCapture) && southWest.isValid()) {
            squares.add(southWest);
            if (canCapture) {
                return;
            }
            addSouthWestSquare(southWest, piece, squares);
        }
        return;
    }

    private static void addSouthEastSquare(Square startingSquare, Piece piece, LinkedList<Square> squares) {
        Square southEast = new Square(startingSquare.getX() + 1, startingSquare.getY() - 1);
        boolean canCapture = piece.canCaptureOtherOnSquare(southEast);
        if ((piece.getBoard().isEmptySquare(southEast) || canCapture) && southEast.isValid()) {
            squares.add(southEast);
            if (canCapture) {
                return;
            }
            addSouthEastSquare(southEast, piece, squares);
        }
        return;
    }

    private static void addWestSquare(Square startingSquare, Piece piece, LinkedList<Square> squares) {
        Square west = new Square(startingSquare.getX() - 1, startingSquare.getY());
        boolean canCapture = piece.canCaptureOtherOnSquare(west);
        if ((piece.getBoard().isEmptySquare(west) || canCapture) && west.isValid()) {
            squares.add(west);
            if (canCapture) {
                return;
            }
            addWestSquare(west, piece, squares);
        }
        return;
    }

    private static void addNorthSquare(Square startingSquare, Piece piece, LinkedList<Square> squares) {
        Square north = new Square(startingSquare.getX(), startingSquare.getY() + 1);
        boolean canCapture = piece.canCaptureOtherOnSquare(north);
        if ((piece.getBoard().isEmptySquare(north) || canCapture) && north.isValid()) {
            squares.add(north);
            if (canCapture) {
                return;
            }
            addNorthSquare(north, piece, squares);
        }
        return;
    }

    private static void addEastSquare(Square startingSquare, Piece piece, LinkedList<Square> squares) {
        Square east = new Square(startingSquare.getX() + 1, startingSquare.getY());
        boolean canCapture = piece.canCaptureOtherOnSquare(east);

        if ((piece.getBoard().isEmptySquare(east) || canCapture) && east.isValid()) {
            squares.add(east);
            if (canCapture) {
                return;
            }
            addEastSquare(east, piece, squares);
        }
        return;
    }

    private static void addSouthSquare(Square startingSquare, Piece piece, LinkedList<Square> squares) {
        Square south = new Square(startingSquare.getX(), startingSquare.getY() - 1);
        boolean canCapture = piece.canCaptureOtherOnSquare(south);

        if ((piece.getBoard().isEmptySquare(south) || canCapture) && south.isValid()) {
            squares.add(south);
            if (canCapture) {
                return;
            }
            addSouthSquare(south, piece, squares);
        }
        return;
    }

    public static boolean isCoveredByOwnPiece(Piece targetPiece, ChessBoard board) {

        Square square = targetPiece.getPosition();

        if (!board.isAttackedSquare(square, targetPiece.getColor().equals(PieceColor.White) ? PieceColor.Black : PieceColor.White)) {
            return true;
        }

        Piece p = board.getPiece(square);
        List<Piece> pieces = new ArrayList<>();
        if (p.getColor() == PieceColor.White) {
            pieces.addAll(board.getWhitePieces());
        } else {
            pieces.addAll(board.getBlackPieces());
        }

        for (Piece piece : pieces) {

            if (piece instanceof Pawn) {
                if (((Pawn) piece).getDirection() == MovementDirection.Up) {
                    isCovered = square.equals(new Square(piece.getPosition().getX() + 1, piece.getPosition().getY() + 1))
                            || square.equals(new Square(piece.getPosition().getX() - 1, piece.getPosition().getY() + 1));
                } else {
                    isCovered = square.equals(new Square(piece.getPosition().getX() + 1, piece.getPosition().getY() - 1))
                            || square.equals(new Square(piece.getPosition().getX() - 1, piece.getPosition().getY() - 1));
                }
                if (isCovered) {
                    isCovered = false;
                    return true;
                }
            } else if (piece instanceof Bishop) {
                if (square.getX() > piece.getPosition().getX() && square.getY() > piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.NorthEast);
                } else if (square.getX() > piece.getPosition().getX() && square.getY() < piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.SouthEast);
                } else if (square.getX() < piece.getPosition().getX() && square.getY() > piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.NorthWest);
                } else if (square.getX() < piece.getPosition().getX() && square.getY() < piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.SouthWest);
                }
                if (isCovered) {
                    isCovered = false;
                    return true;
                }
            } else if (piece instanceof Knight) {
                if (isInKnightAttackingSquares(square, piece)) {
                    return true;
                }
            } else if (piece instanceof Rook) {
                if (square.getX() == piece.getPosition().getX() && square.getY() > piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.North);
                } else if (square.getX() == piece.getPosition().getX() && square.getY() < piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.South);
                } else if (square.getX() < piece.getPosition().getX() && square.getY() == piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.West);
                } else if (square.getX() > piece.getPosition().getX() && square.getY() == piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.East);
                }
                if (isCovered) {
                    isCovered = false;
                    return true;
                }
            } else if (piece instanceof Queen) {
                if (square.getX() > piece.getPosition().getX() && square.getY() > piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.NorthEast);
                } else if (square.getX() > piece.getPosition().getX() && square.getY() < piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.SouthEast);
                } else if (square.getX() < piece.getPosition().getX() && square.getY() > piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.NorthWest);
                } else if (square.getX() < piece.getPosition().getX() && square.getY() < piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.SouthWest);
                } else if (square.getX() == piece.getPosition().getX() && square.getY() > piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.North);
                } else if (square.getX() == piece.getPosition().getX() && square.getY() < piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.South);
                } else if (square.getX() < piece.getPosition().getX() && square.getY() == piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.West);
                } else if (square.getX() > piece.getPosition().getX() && square.getY() == piece.getPosition().getY()) {
                    checkSquare(square, piece, board, Direction.East);
                }
                if (isCovered) {
                    isCovered = false;
                    return true;
                }
            }
        }
        return isCovered;
    }

    private static boolean isInKnightAttackingSquares(Square square, Piece piece) {
        return square.equals(new Square(piece.getPosition().getX() + 2, piece.getPosition().getY() + 1))
                || square.equals(new Square(piece.getPosition().getX() + 1, piece.getPosition().getY() + 2))
                || square.equals(new Square(piece.getPosition().getX() - 2, piece.getPosition().getY() + 1))
                || square.equals(new Square(piece.getPosition().getX() - 1, piece.getPosition().getY() + 2))
                || square.equals(new Square(piece.getPosition().getX() - 2, piece.getPosition().getY() - 1))
                || square.equals(new Square(piece.getPosition().getX() + 2, piece.getPosition().getY() - 1))
                || square.equals(new Square(piece.getPosition().getX() - 1, piece.getPosition().getY() - 2))
                || square.equals(new Square(piece.getPosition().getX() + 1, piece.getPosition().getY() - 2));
    }

    public static boolean playerCanBlockChessPath(Piece piece, Square attackedKingPosition) {
        List<Piece> pieces = new ArrayList<>();
        if (piece.getColor() == PieceColor.Black) {
            pieces.addAll(piece.getBoard().getWhitePieces());
        } else {
            pieces.addAll(piece.getBoard().getBlackPieces());
        }

        for (Piece opponentPiece : pieces) {
            if (opponentPiece.equals(piece)) {
                continue;
            }

            if (piece instanceof Bishop) {
                if (attackedKingPosition.getX() > piece.getPosition().getX() && attackedKingPosition.getY() > piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.NorthEast);
                } else if (attackedKingPosition.getX() > piece.getPosition().getX() && attackedKingPosition.getY() < piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.SouthEast);
                } else if (attackedKingPosition.getX() < piece.getPosition().getX() && attackedKingPosition.getY() > piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.NorthWest);
                } else if (attackedKingPosition.getX() < piece.getPosition().getX() && attackedKingPosition.getY() < piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.SouthWest);
                }
                if (isCovered) {
                    isCovered = false;
                    return true;
                }
            } else if (piece instanceof Rook) {
                if (attackedKingPosition.getX() == piece.getPosition().getX() && attackedKingPosition.getY() > piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.North);
                } else if (attackedKingPosition.getX() == piece.getPosition().getX() && attackedKingPosition.getY() < piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.South);
                } else if (attackedKingPosition.getX() < piece.getPosition().getX() && attackedKingPosition.getY() == piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.West);
                } else if (attackedKingPosition.getX() > piece.getPosition().getX() && attackedKingPosition.getY() == piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.East);
                }
                if (isCovered) {
                    isCovered = false;
                    return true;
                }
            } else if (piece instanceof Queen) {
                if (attackedKingPosition.getX() > piece.getPosition().getX() && attackedKingPosition.getY() > piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.NorthEast);
                } else if (attackedKingPosition.getX() > piece.getPosition().getX() && attackedKingPosition.getY() < piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.SouthEast);
                } else if (attackedKingPosition.getX() < piece.getPosition().getX() && attackedKingPosition.getY() > piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.NorthWest);
                } else if (attackedKingPosition.getX() < piece.getPosition().getX() && attackedKingPosition.getY() < piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.SouthWest);
                } else if (attackedKingPosition.getX() == piece.getPosition().getX() && attackedKingPosition.getY() > piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.North);
                } else if (attackedKingPosition.getX() == piece.getPosition().getX() && attackedKingPosition.getY() < piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.South);
                } else if (attackedKingPosition.getX() < piece.getPosition().getX() && attackedKingPosition.getY() == piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.West);
                } else if (attackedKingPosition.getX() > piece.getPosition().getX() && attackedKingPosition.getY() == piece.getPosition().getY()) {
                    checkIfSquareIsBlocked(attackedKingPosition, opponentPiece, piece.getPosition(), Direction.East);
                }
                if (isCovered) {
                    isCovered = false;
                    return true;
                }
            }

        }
        return isCovered;
    }

    private static void checkIfSquareIsBlocked(Square attackedKingPosition, Piece p, Square startingSquare, Direction dir) {
        Square s = getNextSquare(startingSquare, dir);

        if (s.equals(attackedKingPosition)) {
            return;
        }
        if (p.validMoves().contains(s)) {
            isCovered = true;
            return;
        }
        checkIfSquareIsBlocked(attackedKingPosition, p, s, dir);
    }

    private static void checkSquare(Square startingSquare, Piece piece, ChessBoard board, Direction dir) {
        Square s = getNextSquare(piece.getPosition(), dir);

        if (s.equals(startingSquare)) {
            isCovered = true;
            return;
        }

        boolean canCapture = piece.canCaptureOtherOnSquare(s);
        if ((piece.getBoard().isEmptySquare(s) || canCapture) && s.isValid()) {
            if (canCapture) {
                return;
            }
            checkSquare(s, piece, board, dir);
        }
    }

    private static Square getNextSquare(Square startingSquare, Direction dir) {
        switch (dir) {
            case East:
                return new Square(startingSquare.getX() + 1, startingSquare.getY());
            case North:
                return new Square(startingSquare.getX(), startingSquare.getY() + 1);
            case South:
                return new Square(startingSquare.getX(), startingSquare.getY() - 1);
            case West:
                return new Square(startingSquare.getX() - 1, startingSquare.getY());
            case NorthEast:
                return new Square(startingSquare.getX() + 1, startingSquare.getY() + 1);
            case SouthEast:
                return new Square(startingSquare.getX() + 1, startingSquare.getY() - 1);
            case NorthWest:
                return new Square(startingSquare.getX() - 1, startingSquare.getY() + 1);
            case SouthWest:
                return new Square(startingSquare.getX() - 1, startingSquare.getY() - 1);
        }
        return null;
    }

    public enum Direction {
        North, South, East, West, NorthEast, SouthEast, NorthWest, SouthWest
    }

}
