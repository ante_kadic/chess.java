package PieceModel;

import Events.CapturedEvent;
import Events.CapturedListener;
import BLL.ChessBoard;
import Enums.PieceColor;
import BLL.Move;
import BLL.Square;
import Helpers.PieceImageManager;
import java.awt.Event;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public abstract class Piece implements Serializable {

    private PieceColor color;
    private Square position;
    private boolean isMoved;
    private static ChessBoard board;
  
    public Piece(PieceColor Color, Square initPosition) {
        this.color = Color;
        setPosition(initPosition);
        isMoved = false;
    }

    public boolean canCaptureOtherOnSquare(Square square) {
        return this.getColor() == PieceColor.Black ? getBoard().isWhiteOccupied(square) : getBoard().isBlackOccupied(square);
    }

    public abstract List<Square> validMoves();

    public boolean isValidMove(Move move) {
        for (Square s : validMoves()) {
            if (s.getX() == move.getDestinationSquare().getX() && s.getY() == move.getDestinationSquare().getY()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return the Color
     */
    public PieceColor getColor() {
        return color;
    }

    /**
     * @return the placeAt
     */
    public Square getPosition() {
        return position;
    }

    public void setPosition(Square square) {
        position = square;
        if (!isIsMoved()) {
            setIsMoved(true);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().equals(this.getClass()) && ((Piece) obj).getColor().equals(this.getColor()) && ((Piece) obj).getPosition().equals(this.getPosition())) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        switch (getColor()) {
            case Black:
                return 1;
            case White:
                return 2;
        }
        return 1;
    }

    public void setColor(PieceColor color) {
        this.color = color;
    }

    /**
     * @return the isMoved
     */
    public boolean isIsMoved() {
        return isMoved;
    }

    public void setIsMoved(boolean isMoved) {
        this.isMoved = isMoved;
    }

    /**
     * @return the board
     */
    public static ChessBoard getBoard() {
        return board;
    }

    public static void setBoard(ChessBoard aBoard) {
        board = aBoard;
    }

}
