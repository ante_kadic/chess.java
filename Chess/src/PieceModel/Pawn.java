/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PieceModel;

import Enums.MovementDirection;
import Enums.PieceColor;
import BLL.Square;
import Helpers.PieceMovementManager;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author ante.kadic
 */
public class Pawn extends Piece implements Serializable {

    private Piece promotedTo;
    private MovementDirection direction;

    public Pawn(PieceColor Color, Square initPosition) {
        super(Color, initPosition);
        direction = Color == PieceColor.White ? MovementDirection.Up : MovementDirection.Down;
    }

    public boolean isPromoted() {
        return promotedTo != null;
    }

    @Override
    public List<Square> validMoves() {
        return PieceMovementManager.getPawnValidMoves(this);
    }

    public List<Square> attackingSquares() {
        return PieceMovementManager.getPawnEmptyAttackingMoves(this);
    }

    /**
     * @return the promotedTo
     */
    public Piece getPromotedTo() {
        return promotedTo;
    }

    /**
     * @param promotedTo the promotedTo to set
     */
    public void setPromotedTo(Piece promotedTo) {
        this.promotedTo = promotedTo;
    }

    /**
     * @return the direction
     */
    public MovementDirection getDirection() {
        return direction;
    }

}
