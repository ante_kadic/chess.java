/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PieceModel;

import Enums.MovementDirection;
import Enums.PieceColor;
import BLL.Square;
import Enums.CheckState;
import Helpers.PieceMovementManager;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class King extends Piece implements Serializable {

    private boolean onCheck;

    public King(PieceColor Color, Square initPosition) {
        super(Color, initPosition);
    }

    @Override
    public List<Square> validMoves() {
        return PieceMovementManager.getKingValidMoves(this);
    }

    @Override
    public boolean canCaptureOtherOnSquare(Square square) {
        boolean isSquareOccupied = this.getColor() == PieceColor.Black ? getBoard().isWhiteOccupied(square) : getBoard().isBlackOccupied(square);

        if (isSquareOccupied) {
            return !PieceMovementManager.isCoveredByOwnPiece(getBoard().getPiece(square), getBoard());
        }
        return false;
    }

    /**
     * @return the onCheck
     */
    public boolean isOnCheck() {
        return onCheck;
    }

    public void setOnCheck(boolean onCheck, Piece opponent) {
        this.onCheck = onCheck;
        
        if (onCheck && validMoves().isEmpty() && PieceMovementManager.isCoveredByOwnPiece(opponent, getBoard()) && !PieceMovementManager.playerCanBlockChessPath(opponent, getPosition())) {
            getBoard().raiseOnCheckStateChangedEvent(getColor(), CheckState.Checkmate);
        } else if (onCheck) {
            getBoard().raiseOnCheckStateChangedEvent(getColor(), CheckState.Check);
        } else {
            getBoard().raiseOnCheckStateChangedEvent(getColor(), CheckState.None);
        }
    }

    public boolean getOnCheck() {
        return this.onCheck;
    }

}
