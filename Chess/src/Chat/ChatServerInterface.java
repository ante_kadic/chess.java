/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Chat;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author ante.kadic
 */
public interface ChatServerInterface extends Remote{
	public void sendMessage(String msg, String nickName) throws RemoteException;
    
}
