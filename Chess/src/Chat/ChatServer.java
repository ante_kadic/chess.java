/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Chat;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author ante.kadic
 */
public class ChatServer implements ChatServerInterface {

    private ChatClientInterface client = null;

    public ChatServer(ChatClient client) throws RemoteException {
            this.client = client;
    }

    public void sendMessage(String s, String name) throws RemoteException {
        client.retrieveMessage(s, name);
    }

}
