/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import BLL.*;
import Chat.ChatManager;
import Enums.*;
import Events.CapturedEvent;
import Events.CapturedListener;
import Events.ChatListener;
import Helpers.*;
import PieceModel.Piece;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.sql.Time;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.border.LineBorder;

/**
 *
 * @author ante.kadic
 */
public class ChessBoardGUI extends javax.swing.JFrame implements CapturedListener, ChatListener {

    private final Color STANDARD_MSG_CLR = Color.getHSBColor(0, 204, 0);
    private final Color AVAILABLE_MOVES_BORDER_CLR = Color.blue;
    private final String PLAYER_ON_MOVE_MSG = "%s is on move";
    private final String PLAYER_WON_MSG = "Game Over, %s WON";
    private final String PLAYER_GAVEUP_MSG = "Game Over, %s WON - %s gave up";
    private final String DRAW_MSG = "Game Over, It's a draw!";
    private final String WAITING_CONNETION_MSG = "Waiting for opponent to connect...";
    private final String SAME_COLOR_ERROR_MSG = "You both pick same piece color. Try again.";
    private final int AVAILABLE_MOVES_BORDER_THICKNESS = 6;
    private final int IMAGE_SIZE = 70;
    private boolean isMultiPlayer = false;
    private Player player;
    private String opponentAddress;
    private String opponentNickName;
    private PieceColor opponentPieceColor;
    private ChatManager chatManager;
    private boolean isClearedBoard = true;
    private Thread gameTimer;
    private MouseAdapter adapter;
    private Piece selectedPiece;
    private ChessGame game;
    private SquarePanelMapper panelManager;
    private HashMap<String, JPanel> panels;

    public ChessBoardGUI() {
        initComponents();
        miDemo.setEnabled(false);

        pnlTimeBlack.setVisible(false);
        pnlTimeWhite.setVisible(false);
        txtChatInput.setEnabled(false);

        btnOptions.setEnabled(false);
        this.setTitle("CHESS");
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        initConfig();
    }

    private void initConfig() {
        createMouseEventHandler();
        loadPanels();

        panelManager = new SquarePanelMapper(panels);
        for (JPanel panel : panels.values()) {
            panel.setBorder(new LineBorder(Color.yellow, 1));
        }
    }

    public void startGame(boolean isNewGame) {
        if (!isClearedBoard) {
            stopGame();
        } else {
            isClearedBoard = false;
        }

        subscribeForMouseEvents();
        drawPieces();

        btnOptions.setEnabled(true);

        pnlTimeBlack.setVisible(true);
        pnlTimeWhite.setVisible(true);
        txtChatInput.setEnabled(true);

        initTimers(isNewGame);
        gameTimer.start();
    }

    public void startGame(boolean isNewGame, String address, String nick, String color) {
        if (!isClearedBoard) {
            stopGame();
        } else {
            isClearedBoard = false;
        }

        opponentAddress = address;
        player = new Player((nick), PieceColor.valueOf(color));
        game = new ChessGame(player, this);
        chatManager = new ChatManager(player.getName(), this);

        writeMessage(new CustomMessage(WAITING_CONNETION_MSG, Color.BLUE));

        tryConnectAsync(isNewGame);

    }

    private void tryConnectAsync(boolean isNewGame) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    GameManager.connectToOpponent(opponentAddress);
                    chatManager.sendMessage(ChatManager.NICKNAME_MESSAGE_FORMAT + player.getName(), opponentAddress);
                    chatManager.sendMessage(ChatManager.PIECECOLOR_MESSAGE_FORMAT + player.getPieceColor(), opponentAddress);

                } catch (IOException ex) {
                    Logger.getLogger(ChessBoardGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                while (opponentPieceColor == null) {
                }
                boolean isSameColor = checkIfSameColor();
                if (isSameColor) {
                    stopGame();
                    return;
                }
                writeMessage(new CustomMessage(String.format(PLAYER_ON_MOVE_MSG, player.getPieceColor().equals(PieceColor.White) ? player.getName() : opponentNickName), STANDARD_MSG_CLR));

                pnlTimeBlack.setVisible(true);
                pnlTimeWhite.setVisible(true);
                txtChatInput.setEnabled(true);
                txtChatInput.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyPressed(KeyEvent e) {
                        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                            e.consume();
                            btnSendMessage.doClick();
                        }
                    }
                });
                initTimers(isNewGame);
                gameTimer.start();
                subscribeForMouseEvents();
                drawPieces();

                btnOptions.setEnabled(true);
                if (player.getPieceColor()
                        == PieceColor.White) {

                    return;
                }
                getStateFromOpponnentAsync();

            }
        });

        t.setDaemon(true);
        t.start();
    }

    private void getStateFromOpponnentAsync() {
        ChessBoardGUI gui = this;
        Thread stateThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ChessGame tempGame = GameManager.getStateFromOpponent(gui);
                    if (tempGame == null) {
                        game.setResult(player.getPieceColor().equals(PieceColor.Black) ? Result.BlackWins : Result.WhiteWins);
                        writeMessage(new CustomMessage(String.format(PLAYER_GAVEUP_MSG, player.getName(), opponentNickName), Color.blue));
                        pauseGame();
                        return;
                    }
                    removePieces();
                    game = tempGame;
                    drawPieces();
                    writeMessage(game.lblMessage);
                } catch (IOException ex) {
                    Logger.getLogger(ChessBoardGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ChessBoardGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        stateThread.setDaemon(
                true);
        stateThread.start();
    }

    private boolean checkIfSameColor() {
        if (player.getPieceColor().equals(opponentPieceColor)) {
            writeMessage(new CustomMessage(SAME_COLOR_ERROR_MSG, Color.RED));
            return true;
        }
        return false;
    }

    public void pauseGame() {

        if (gameTimer != null) {
            stopTimer();
        }
        GameManager.disconnectFromOpponent();

        disableBoardOnClick();
        btnNewGame.setEnabled(true);
    }

    public void stopGame() {
        if (gameTimer != null) {
            stopTimer();
        }
        disableBoardOnClick();

        GameManager.disconnectFromOpponent();

        removePieces();
        btnNewGame.setEnabled(true);
        isClearedBoard = true;
    }

    private void disableBoardOnClick() {
        game.removeListeners(this);

        for (JPanel panel
                : panels.values()) {
            panel.removeMouseListener(adapter);
        }
    }

    private void removePieces() {
        for (Piece piece : game.getBoard().getBlackPieces()) {
            removeImage(panelManager.getPanel(piece.getPosition()));
        }
        for (Piece piece : game.getBoard().getWhitePieces()) {
            removeImage(panelManager.getPanel(piece.getPosition()));
        }
    }

    private void drawPieces() {
        for (Piece piece : game.getBoard().getBlackPieces()) {
            drawPiece(piece);
        }
        for (Piece piece : game.getBoard().getWhitePieces()) {
            drawPiece(piece);
        }
    }

    private void subscribeForMouseEvents() {
        for (JPanel panel : panels.values()) {
            panel.addMouseListener(adapter);
        }
    }

    private void removeImage(JPanel panel) {
        panel.removeAll();

        panel.revalidate();
        panel.repaint();
    }

    private void makeMove(Square s) {

        Square startPosition = selectedPiece.getPosition();
        if (game.makeMove(selectedPiece, s)) {
            removeImage(panelManager.getPanel(startPosition));
            drawPiece(selectedPiece);
            if (game.getCheckState() == CheckState.None) {
                String msg = player.getPieceColor() == game.getPlayerTurn() ? String.format(PLAYER_ON_MOVE_MSG, player.getName()) : String.format(PLAYER_ON_MOVE_MSG, opponentNickName);
                writeMessage(new CustomMessage(msg, STANDARD_MSG_CLR));
            }

            unmarkAvailablePanels();
            selectedPiece = null;

            pnlBoard.revalidate();
            pnlBoard.repaint();

            if (isMultiPlayer) {
                ChessBoardGUI guiListener = this;
                Thread stateSender = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            try {
                                GameManager.sendStateToOpponent(game);
                            } catch (Exception ex) {
                                game.setResult(player.getPieceColor().equals(PieceColor.Black) ? Result.BlackWins : Result.WhiteWins);
                                writeMessage(new CustomMessage(String.format(PLAYER_GAVEUP_MSG, player.getName(), opponentNickName), Color.blue));
                                pauseGame();
                                return;
                            }
                            ChessGame tempGame = GameManager.getStateFromOpponent(guiListener);
                            if (tempGame == null) {
                                game.setResult(player.getPieceColor().equals(PieceColor.Black) ? Result.BlackWins : Result.WhiteWins);
                                writeMessage(new CustomMessage(String.format(PLAYER_GAVEUP_MSG, player.getName(), opponentNickName), Color.blue));
                                pauseGame();
                                return;
                            }
                            removePieces();
                            game = tempGame;
                            writeMessage(game.lblMessage);
                            drawPieces();
                            if (game.getResult() != Result.None) {
                                pauseGame();
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(ChessBoardGUI.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (ClassNotFoundException ex) {
                            Logger.getLogger(ChessBoardGUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
                stateSender.setDaemon(true);
                stateSender.start();
            }
        }

    }

    private void drawPiece(Piece piece) {
        ImageIcon icon = PieceImageManager.getImage(piece);

        JPanel panel = panelManager.getPanel(piece.getPosition());

        java.awt.Image img = icon.getImage();
        java.awt.Image newImage = img.getScaledInstance(IMAGE_SIZE, IMAGE_SIZE, java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newImage);
        JLabel label = new JLabel(icon);

        label.setSize(IMAGE_SIZE, IMAGE_SIZE);

        panel.setLayout(new BorderLayout());
        panel.add(label, BorderLayout.CENTER);

        panel.revalidate();
        panel.repaint();
    }

    private void markAvailablePanels(Piece piece) {
        if (piece == null) {
            return;
        }
        LinkedList<Square> list = (LinkedList<Square>) piece.validMoves();
        for (Square sq : list) {
            panelManager.getPanel(sq).setBorder(new LineBorder(AVAILABLE_MOVES_BORDER_CLR, AVAILABLE_MOVES_BORDER_THICKNESS));
        }
    }

    private void unmarkAvailablePanels() {
        for (JPanel jPanel : panels.values()) {
            jPanel.setBorder(new LineBorder(Color.yellow, 1));
        }
    }

    @Override
    public void onCapturedEvent(CapturedEvent ev) {
        Piece piece = (Piece) ev.getSource();
        removeImage(panelManager.getPanel(piece.getPosition()));
    }

    @Override
    public void onCheckStateChangedEvent(PieceColor playerOnCheckState, CheckState state) {
        switch (state) {
            case Remi:
            case Timeout:
            case Checkmate:
                writeMessage(new CustomMessage(getWinner(), Color.BLUE));
                pauseGame();
                break;
            case Check:
                String str = playerOnCheckState.name().equals(player.getPieceColor()) ? player.getName() : opponentNickName;
                writeMessage(new CustomMessage(str + " is on " + state.name(), Color.red));
                break;
            default:
                break;
        }
    }

    @Override
    public void onRecievedMessage(String message, String nickName) {
        if (message.startsWith(ChatManager.NICKNAME_MESSAGE_FORMAT)) {
            opponentNickName = message.substring(ChatManager.NICKNAME_MESSAGE_FORMAT.length(), message.length());
            if (player.getPieceColor().equals(PieceColor.Black)) {
                lblNickWhite.setText(opponentNickName);
                lblNickBlack.setText(player.getName());
            } else {
                lblNickBlack.setText(opponentNickName);
                lblNickWhite.setText(player.getName());
            }
        } else if (message.startsWith(ChatManager.PIECECOLOR_MESSAGE_FORMAT)) {
            if (message.substring(ChatManager.PIECECOLOR_MESSAGE_FORMAT.length(), message.length()).equals(PieceColor.White.toString())) {
                opponentPieceColor = PieceColor.White;
            } else if (message.substring(ChatManager.PIECECOLOR_MESSAGE_FORMAT.length(), message.length()).equals(PieceColor.Black.toString())) {
                opponentPieceColor = PieceColor.Black;
            }
        } else {
            writeChatMessage(message, nickName);
        }
    }

    public String getWinner() {
        switch (game.getResult()) {
            case BlackWins:
                return String.format(PLAYER_WON_MSG, player.getPieceColor().equals(PieceColor.Black) ? player.getName() : opponentNickName);
            case WhiteWins:
                return String.format(PLAYER_WON_MSG, player.getPieceColor().equals(PieceColor.White) ? player.getName() : opponentNickName);
            case Draw:
                return DRAW_MSG;
        }
        return "";
    }

    public void writeMessage(CustomMessage msg) {
        lblInfo.setForeground(msg.getFontColor());
        lblInfo.setText(msg.getMessage());
        game.lblMessage = msg;
//        lblInfo.paintImmediately(lblInfo.getVisibleRect());
        lblInfo.repaint();
        lblInfo.revalidate();

    }

    private void stopTimer() {
        while (true) {
            if (gameTimer.getState() != Thread.State.TIMED_WAITING) {
                gameTimer.interrupt();
                break;
            }
        }

        resetTimers();

        lblTimeWhite.setText(String.format("%s : %s", game.whiteMinutes, game.whiteSeconds < 10 ? "0" + String.valueOf(game.whiteSeconds) : String.valueOf(game.whiteSeconds)));
        lblTimeBlack.setText(String.format("%s : %s", game.blackMinutes, game.blackSeconds < 10 ? "0" + String.valueOf(game.blackSeconds) : String.valueOf(game.blackSeconds)));
    }

    private void resetTimers() {
        if (game.INIT_TIME_PLAY == 0) {
            game.INIT_TIME_PLAY = 20;
        }
        game.whiteMinutes = game.INIT_TIME_PLAY - 1;
        game.whiteSeconds = 59;
        game.blackMinutes = game.INIT_TIME_PLAY - 1;
        game.blackSeconds = 59;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem1 = new javax.swing.JMenuItem();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem2 = new javax.swing.JCheckBoxMenuItem();
        pnlBoard = new javax.swing.JPanel();
        square07 = new javax.swing.JPanel();
        square17 = new javax.swing.JPanel();
        square27 = new javax.swing.JPanel();
        square37 = new javax.swing.JPanel();
        square47 = new javax.swing.JPanel();
        square57 = new javax.swing.JPanel();
        square67 = new javax.swing.JPanel();
        square77 = new javax.swing.JPanel();
        square06 = new javax.swing.JPanel();
        square16 = new javax.swing.JPanel();
        square26 = new javax.swing.JPanel();
        square36 = new javax.swing.JPanel();
        square46 = new javax.swing.JPanel();
        square56 = new javax.swing.JPanel();
        square66 = new javax.swing.JPanel();
        square76 = new javax.swing.JPanel();
        square05 = new javax.swing.JPanel();
        square15 = new javax.swing.JPanel();
        square25 = new javax.swing.JPanel();
        square35 = new javax.swing.JPanel();
        square45 = new javax.swing.JPanel();
        square55 = new javax.swing.JPanel();
        square65 = new javax.swing.JPanel();
        square75 = new javax.swing.JPanel();
        square04 = new javax.swing.JPanel();
        square14 = new javax.swing.JPanel();
        square24 = new javax.swing.JPanel();
        square34 = new javax.swing.JPanel();
        square44 = new javax.swing.JPanel();
        square54 = new javax.swing.JPanel();
        square64 = new javax.swing.JPanel();
        square74 = new javax.swing.JPanel();
        square03 = new javax.swing.JPanel();
        square13 = new javax.swing.JPanel();
        square23 = new javax.swing.JPanel();
        square33 = new javax.swing.JPanel();
        square43 = new javax.swing.JPanel();
        square53 = new javax.swing.JPanel();
        square63 = new javax.swing.JPanel();
        square73 = new javax.swing.JPanel();
        square02 = new javax.swing.JPanel();
        square12 = new javax.swing.JPanel();
        square22 = new javax.swing.JPanel();
        square32 = new javax.swing.JPanel();
        square42 = new javax.swing.JPanel();
        square52 = new javax.swing.JPanel();
        square62 = new javax.swing.JPanel();
        square72 = new javax.swing.JPanel();
        square01 = new javax.swing.JPanel();
        square11 = new javax.swing.JPanel();
        square21 = new javax.swing.JPanel();
        square31 = new javax.swing.JPanel();
        square41 = new javax.swing.JPanel();
        square51 = new javax.swing.JPanel();
        square61 = new javax.swing.JPanel();
        square71 = new javax.swing.JPanel();
        square00 = new javax.swing.JPanel();
        square10 = new javax.swing.JPanel();
        square20 = new javax.swing.JPanel();
        square30 = new javax.swing.JPanel();
        square40 = new javax.swing.JPanel();
        square50 = new javax.swing.JPanel();
        square60 = new javax.swing.JPanel();
        square70 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        lblInfo = new javax.swing.JLabel();
        pnlTimeWhite = new javax.swing.JPanel();
        lblTimeWhite = new javax.swing.JLabel();
        lblNickWhite = new javax.swing.JLabel();
        pnlTimeBlack = new javax.swing.JPanel();
        lblTimeBlack = new javax.swing.JLabel();
        lblNickBlack = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtChatView = new javax.swing.JTextArea();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtChatInput = new javax.swing.JTextArea();
        btnSendMessage = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        btnNewGame = new javax.swing.JMenu();
        miMultiPlr = new javax.swing.JMenuItem();
        miDemo = new javax.swing.JMenuItem();
        btnLoadGame = new javax.swing.JMenuItem();
        btnOptions = new javax.swing.JMenu();
        btnSurrender = new javax.swing.JMenuItem();
        btnSaveGame = new javax.swing.JMenuItem();

        jMenuItem1.setText("jMenuItem1");

        jCheckBoxMenuItem1.setSelected(true);
        jCheckBoxMenuItem1.setText("jCheckBoxMenuItem1");

        jCheckBoxMenuItem2.setSelected(true);
        jCheckBoxMenuItem2.setText("jCheckBoxMenuItem2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pnlBoard.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pnlBoard.setLayout(new java.awt.GridLayout(8, 8));

        javax.swing.GroupLayout square07Layout = new javax.swing.GroupLayout(square07);
        square07.setLayout(square07Layout);
        square07Layout.setHorizontalGroup(
            square07Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square07Layout.setVerticalGroup(
            square07Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square07);

        square17.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square17Layout = new javax.swing.GroupLayout(square17);
        square17.setLayout(square17Layout);
        square17Layout.setHorizontalGroup(
            square17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square17Layout.setVerticalGroup(
            square17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square17);

        javax.swing.GroupLayout square27Layout = new javax.swing.GroupLayout(square27);
        square27.setLayout(square27Layout);
        square27Layout.setHorizontalGroup(
            square27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square27Layout.setVerticalGroup(
            square27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square27);

        square37.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square37Layout = new javax.swing.GroupLayout(square37);
        square37.setLayout(square37Layout);
        square37Layout.setHorizontalGroup(
            square37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square37Layout.setVerticalGroup(
            square37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square37);

        javax.swing.GroupLayout square47Layout = new javax.swing.GroupLayout(square47);
        square47.setLayout(square47Layout);
        square47Layout.setHorizontalGroup(
            square47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square47Layout.setVerticalGroup(
            square47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square47);

        square57.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square57Layout = new javax.swing.GroupLayout(square57);
        square57.setLayout(square57Layout);
        square57Layout.setHorizontalGroup(
            square57Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square57Layout.setVerticalGroup(
            square57Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square57);

        javax.swing.GroupLayout square67Layout = new javax.swing.GroupLayout(square67);
        square67.setLayout(square67Layout);
        square67Layout.setHorizontalGroup(
            square67Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square67Layout.setVerticalGroup(
            square67Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square67);

        square77.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square77Layout = new javax.swing.GroupLayout(square77);
        square77.setLayout(square77Layout);
        square77Layout.setHorizontalGroup(
            square77Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square77Layout.setVerticalGroup(
            square77Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square77);

        square06.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square06Layout = new javax.swing.GroupLayout(square06);
        square06.setLayout(square06Layout);
        square06Layout.setHorizontalGroup(
            square06Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square06Layout.setVerticalGroup(
            square06Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square06);

        javax.swing.GroupLayout square16Layout = new javax.swing.GroupLayout(square16);
        square16.setLayout(square16Layout);
        square16Layout.setHorizontalGroup(
            square16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square16Layout.setVerticalGroup(
            square16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square16);

        square26.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square26Layout = new javax.swing.GroupLayout(square26);
        square26.setLayout(square26Layout);
        square26Layout.setHorizontalGroup(
            square26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square26Layout.setVerticalGroup(
            square26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square26);

        javax.swing.GroupLayout square36Layout = new javax.swing.GroupLayout(square36);
        square36.setLayout(square36Layout);
        square36Layout.setHorizontalGroup(
            square36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square36Layout.setVerticalGroup(
            square36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square36);

        square46.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square46Layout = new javax.swing.GroupLayout(square46);
        square46.setLayout(square46Layout);
        square46Layout.setHorizontalGroup(
            square46Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square46Layout.setVerticalGroup(
            square46Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square46);

        javax.swing.GroupLayout square56Layout = new javax.swing.GroupLayout(square56);
        square56.setLayout(square56Layout);
        square56Layout.setHorizontalGroup(
            square56Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square56Layout.setVerticalGroup(
            square56Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square56);

        square66.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square66Layout = new javax.swing.GroupLayout(square66);
        square66.setLayout(square66Layout);
        square66Layout.setHorizontalGroup(
            square66Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square66Layout.setVerticalGroup(
            square66Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square66);

        javax.swing.GroupLayout square76Layout = new javax.swing.GroupLayout(square76);
        square76.setLayout(square76Layout);
        square76Layout.setHorizontalGroup(
            square76Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square76Layout.setVerticalGroup(
            square76Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square76);

        javax.swing.GroupLayout square05Layout = new javax.swing.GroupLayout(square05);
        square05.setLayout(square05Layout);
        square05Layout.setHorizontalGroup(
            square05Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square05Layout.setVerticalGroup(
            square05Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square05);

        square15.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square15Layout = new javax.swing.GroupLayout(square15);
        square15.setLayout(square15Layout);
        square15Layout.setHorizontalGroup(
            square15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square15Layout.setVerticalGroup(
            square15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square15);

        javax.swing.GroupLayout square25Layout = new javax.swing.GroupLayout(square25);
        square25.setLayout(square25Layout);
        square25Layout.setHorizontalGroup(
            square25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square25Layout.setVerticalGroup(
            square25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square25);

        square35.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square35Layout = new javax.swing.GroupLayout(square35);
        square35.setLayout(square35Layout);
        square35Layout.setHorizontalGroup(
            square35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square35Layout.setVerticalGroup(
            square35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square35);

        javax.swing.GroupLayout square45Layout = new javax.swing.GroupLayout(square45);
        square45.setLayout(square45Layout);
        square45Layout.setHorizontalGroup(
            square45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square45Layout.setVerticalGroup(
            square45Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square45);

        square55.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square55Layout = new javax.swing.GroupLayout(square55);
        square55.setLayout(square55Layout);
        square55Layout.setHorizontalGroup(
            square55Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square55Layout.setVerticalGroup(
            square55Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square55);

        javax.swing.GroupLayout square65Layout = new javax.swing.GroupLayout(square65);
        square65.setLayout(square65Layout);
        square65Layout.setHorizontalGroup(
            square65Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square65Layout.setVerticalGroup(
            square65Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square65);

        square75.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square75Layout = new javax.swing.GroupLayout(square75);
        square75.setLayout(square75Layout);
        square75Layout.setHorizontalGroup(
            square75Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square75Layout.setVerticalGroup(
            square75Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square75);

        square04.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square04Layout = new javax.swing.GroupLayout(square04);
        square04.setLayout(square04Layout);
        square04Layout.setHorizontalGroup(
            square04Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square04Layout.setVerticalGroup(
            square04Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square04);

        javax.swing.GroupLayout square14Layout = new javax.swing.GroupLayout(square14);
        square14.setLayout(square14Layout);
        square14Layout.setHorizontalGroup(
            square14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square14Layout.setVerticalGroup(
            square14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square14);

        square24.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square24Layout = new javax.swing.GroupLayout(square24);
        square24.setLayout(square24Layout);
        square24Layout.setHorizontalGroup(
            square24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square24Layout.setVerticalGroup(
            square24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square24);

        javax.swing.GroupLayout square34Layout = new javax.swing.GroupLayout(square34);
        square34.setLayout(square34Layout);
        square34Layout.setHorizontalGroup(
            square34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square34Layout.setVerticalGroup(
            square34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square34);

        square44.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square44Layout = new javax.swing.GroupLayout(square44);
        square44.setLayout(square44Layout);
        square44Layout.setHorizontalGroup(
            square44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square44Layout.setVerticalGroup(
            square44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square44);

        javax.swing.GroupLayout square54Layout = new javax.swing.GroupLayout(square54);
        square54.setLayout(square54Layout);
        square54Layout.setHorizontalGroup(
            square54Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square54Layout.setVerticalGroup(
            square54Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square54);

        square64.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square64Layout = new javax.swing.GroupLayout(square64);
        square64.setLayout(square64Layout);
        square64Layout.setHorizontalGroup(
            square64Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square64Layout.setVerticalGroup(
            square64Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square64);

        javax.swing.GroupLayout square74Layout = new javax.swing.GroupLayout(square74);
        square74.setLayout(square74Layout);
        square74Layout.setHorizontalGroup(
            square74Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square74Layout.setVerticalGroup(
            square74Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square74);

        javax.swing.GroupLayout square03Layout = new javax.swing.GroupLayout(square03);
        square03.setLayout(square03Layout);
        square03Layout.setHorizontalGroup(
            square03Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square03Layout.setVerticalGroup(
            square03Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square03);

        square13.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square13Layout = new javax.swing.GroupLayout(square13);
        square13.setLayout(square13Layout);
        square13Layout.setHorizontalGroup(
            square13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square13Layout.setVerticalGroup(
            square13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square13);

        javax.swing.GroupLayout square23Layout = new javax.swing.GroupLayout(square23);
        square23.setLayout(square23Layout);
        square23Layout.setHorizontalGroup(
            square23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square23Layout.setVerticalGroup(
            square23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square23);

        square33.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square33Layout = new javax.swing.GroupLayout(square33);
        square33.setLayout(square33Layout);
        square33Layout.setHorizontalGroup(
            square33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square33Layout.setVerticalGroup(
            square33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square33);

        javax.swing.GroupLayout square43Layout = new javax.swing.GroupLayout(square43);
        square43.setLayout(square43Layout);
        square43Layout.setHorizontalGroup(
            square43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square43Layout.setVerticalGroup(
            square43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square43);

        square53.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square53Layout = new javax.swing.GroupLayout(square53);
        square53.setLayout(square53Layout);
        square53Layout.setHorizontalGroup(
            square53Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square53Layout.setVerticalGroup(
            square53Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square53);

        javax.swing.GroupLayout square63Layout = new javax.swing.GroupLayout(square63);
        square63.setLayout(square63Layout);
        square63Layout.setHorizontalGroup(
            square63Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square63Layout.setVerticalGroup(
            square63Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square63);

        square73.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square73Layout = new javax.swing.GroupLayout(square73);
        square73.setLayout(square73Layout);
        square73Layout.setHorizontalGroup(
            square73Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square73Layout.setVerticalGroup(
            square73Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square73);

        square02.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square02Layout = new javax.swing.GroupLayout(square02);
        square02.setLayout(square02Layout);
        square02Layout.setHorizontalGroup(
            square02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square02Layout.setVerticalGroup(
            square02Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square02);

        javax.swing.GroupLayout square12Layout = new javax.swing.GroupLayout(square12);
        square12.setLayout(square12Layout);
        square12Layout.setHorizontalGroup(
            square12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square12Layout.setVerticalGroup(
            square12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square12);

        square22.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square22Layout = new javax.swing.GroupLayout(square22);
        square22.setLayout(square22Layout);
        square22Layout.setHorizontalGroup(
            square22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square22Layout.setVerticalGroup(
            square22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square22);

        javax.swing.GroupLayout square32Layout = new javax.swing.GroupLayout(square32);
        square32.setLayout(square32Layout);
        square32Layout.setHorizontalGroup(
            square32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square32Layout.setVerticalGroup(
            square32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square32);

        square42.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square42Layout = new javax.swing.GroupLayout(square42);
        square42.setLayout(square42Layout);
        square42Layout.setHorizontalGroup(
            square42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square42Layout.setVerticalGroup(
            square42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square42);

        javax.swing.GroupLayout square52Layout = new javax.swing.GroupLayout(square52);
        square52.setLayout(square52Layout);
        square52Layout.setHorizontalGroup(
            square52Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square52Layout.setVerticalGroup(
            square52Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square52);

        square62.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square62Layout = new javax.swing.GroupLayout(square62);
        square62.setLayout(square62Layout);
        square62Layout.setHorizontalGroup(
            square62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square62Layout.setVerticalGroup(
            square62Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square62);

        javax.swing.GroupLayout square72Layout = new javax.swing.GroupLayout(square72);
        square72.setLayout(square72Layout);
        square72Layout.setHorizontalGroup(
            square72Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square72Layout.setVerticalGroup(
            square72Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square72);

        javax.swing.GroupLayout square01Layout = new javax.swing.GroupLayout(square01);
        square01.setLayout(square01Layout);
        square01Layout.setHorizontalGroup(
            square01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square01Layout.setVerticalGroup(
            square01Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square01);

        square11.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square11Layout = new javax.swing.GroupLayout(square11);
        square11.setLayout(square11Layout);
        square11Layout.setHorizontalGroup(
            square11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square11Layout.setVerticalGroup(
            square11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square11);

        javax.swing.GroupLayout square21Layout = new javax.swing.GroupLayout(square21);
        square21.setLayout(square21Layout);
        square21Layout.setHorizontalGroup(
            square21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square21Layout.setVerticalGroup(
            square21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square21);

        square31.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square31Layout = new javax.swing.GroupLayout(square31);
        square31.setLayout(square31Layout);
        square31Layout.setHorizontalGroup(
            square31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square31Layout.setVerticalGroup(
            square31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square31);

        javax.swing.GroupLayout square41Layout = new javax.swing.GroupLayout(square41);
        square41.setLayout(square41Layout);
        square41Layout.setHorizontalGroup(
            square41Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square41Layout.setVerticalGroup(
            square41Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square41);

        square51.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square51Layout = new javax.swing.GroupLayout(square51);
        square51.setLayout(square51Layout);
        square51Layout.setHorizontalGroup(
            square51Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square51Layout.setVerticalGroup(
            square51Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square51);

        javax.swing.GroupLayout square61Layout = new javax.swing.GroupLayout(square61);
        square61.setLayout(square61Layout);
        square61Layout.setHorizontalGroup(
            square61Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square61Layout.setVerticalGroup(
            square61Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square61);

        square71.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square71Layout = new javax.swing.GroupLayout(square71);
        square71.setLayout(square71Layout);
        square71Layout.setHorizontalGroup(
            square71Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square71Layout.setVerticalGroup(
            square71Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square71);

        square00.setBackground(new java.awt.Color(0, 0, 0));
        square00.setName("square00"); // NOI18N

        javax.swing.GroupLayout square00Layout = new javax.swing.GroupLayout(square00);
        square00.setLayout(square00Layout);
        square00Layout.setHorizontalGroup(
            square00Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square00Layout.setVerticalGroup(
            square00Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square00);

        javax.swing.GroupLayout square10Layout = new javax.swing.GroupLayout(square10);
        square10.setLayout(square10Layout);
        square10Layout.setHorizontalGroup(
            square10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square10Layout.setVerticalGroup(
            square10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square10);

        square20.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square20Layout = new javax.swing.GroupLayout(square20);
        square20.setLayout(square20Layout);
        square20Layout.setHorizontalGroup(
            square20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square20Layout.setVerticalGroup(
            square20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square20);

        javax.swing.GroupLayout square30Layout = new javax.swing.GroupLayout(square30);
        square30.setLayout(square30Layout);
        square30Layout.setHorizontalGroup(
            square30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square30Layout.setVerticalGroup(
            square30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square30);

        square40.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square40Layout = new javax.swing.GroupLayout(square40);
        square40.setLayout(square40Layout);
        square40Layout.setHorizontalGroup(
            square40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square40Layout.setVerticalGroup(
            square40Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square40);

        javax.swing.GroupLayout square50Layout = new javax.swing.GroupLayout(square50);
        square50.setLayout(square50Layout);
        square50Layout.setHorizontalGroup(
            square50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square50Layout.setVerticalGroup(
            square50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square50);

        square60.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout square60Layout = new javax.swing.GroupLayout(square60);
        square60.setLayout(square60Layout);
        square60Layout.setHorizontalGroup(
            square60Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square60Layout.setVerticalGroup(
            square60Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square60);

        javax.swing.GroupLayout square70Layout = new javax.swing.GroupLayout(square70);
        square70.setLayout(square70Layout);
        square70Layout.setHorizontalGroup(
            square70Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        square70Layout.setVerticalGroup(
            square70Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 83, Short.MAX_VALUE)
        );

        pnlBoard.add(square70);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("1");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("2");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("3");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("4");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel5.setText("5");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setText("6");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel7.setText("7");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel8.setText("8");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel9.setText("A");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel10.setText("B");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel11.setText("C");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel12.setText("D");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel13.setText("E");

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel14.setText("F");

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel15.setText("G");

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel16.setText("H");

        lblInfo.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lblInfo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblInfo.setOpaque(true);

        pnlTimeWhite.setBackground(new java.awt.Color(153, 153, 153));

        lblTimeWhite.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lblTimeWhite.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lblNickWhite.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNickWhite.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNickWhite.setText("White");

        javax.swing.GroupLayout pnlTimeWhiteLayout = new javax.swing.GroupLayout(pnlTimeWhite);
        pnlTimeWhite.setLayout(pnlTimeWhiteLayout);
        pnlTimeWhiteLayout.setHorizontalGroup(
            pnlTimeWhiteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTimeWhiteLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTimeWhite, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(pnlTimeWhiteLayout.createSequentialGroup()
                .addGap(69, 69, 69)
                .addComponent(lblNickWhite, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlTimeWhiteLayout.setVerticalGroup(
            pnlTimeWhiteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTimeWhiteLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblNickWhite, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(lblTimeWhite, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        pnlTimeBlack.setBackground(new java.awt.Color(153, 153, 153));

        lblTimeBlack.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lblTimeBlack.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lblNickBlack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNickBlack.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNickBlack.setText("Black");

        javax.swing.GroupLayout pnlTimeBlackLayout = new javax.swing.GroupLayout(pnlTimeBlack);
        pnlTimeBlack.setLayout(pnlTimeBlackLayout);
        pnlTimeBlackLayout.setHorizontalGroup(
            pnlTimeBlackLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTimeBlackLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTimeBlack, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(pnlTimeBlackLayout.createSequentialGroup()
                .addGap(64, 64, 64)
                .addComponent(lblNickBlack, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlTimeBlackLayout.setVerticalGroup(
            pnlTimeBlackLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTimeBlackLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(lblTimeBlack, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblNickBlack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        txtChatView.setColumns(20);
        txtChatView.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtChatView.setForeground(new java.awt.Color(51, 51, 51));
        txtChatView.setLineWrap(true);
        txtChatView.setRows(5);
        txtChatView.setEnabled(false);
        jScrollPane2.setViewportView(txtChatView);

        txtChatInput.setColumns(20);
        txtChatInput.setLineWrap(true);
        txtChatInput.setRows(5);
        txtChatInput.setAutoscrolls(false);
        jScrollPane1.setViewportView(txtChatInput);

        btnSendMessage.setText("Send message");
        btnSendMessage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendMessageActionPerformed(evt);
            }
        });

        btnNewGame.setText("New Game");

        miMultiPlr.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        miMultiPlr.setText("Multiplayer");
        miMultiPlr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miMultiPlrActionPerformed(evt);
            }
        });
        btnNewGame.add(miMultiPlr);

        miDemo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        miDemo.setText("Demo");
        miDemo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miDemoActionPerformed(evt);
            }
        });
        btnNewGame.add(miDemo);

        btnLoadGame.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        btnLoadGame.setText("Load last game");
        btnLoadGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoadGameActionPerformed(evt);
            }
        });
        btnNewGame.add(btnLoadGame);

        jMenuBar1.add(btnNewGame);

        btnOptions.setText("Options");

        btnSurrender.setText("Quit");
        btnSurrender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSurrenderActionPerformed(evt);
            }
        });
        btnOptions.add(btnSurrender);

        btnSaveGame.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        btnSaveGame.setText("Save and quit");
        btnSaveGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveGameActionPerformed(evt);
            }
        });
        btnOptions.add(btnSaveGame);

        jMenuBar1.add(btnOptions);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(66, 66, 66)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(pnlBoard, javax.swing.GroupLayout.PREFERRED_SIZE, 812, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(71, 71, 71)
                                .addComponent(jLabel9)
                                .addGap(88, 88, 88)
                                .addComponent(jLabel10)
                                .addGap(85, 85, 85)
                                .addComponent(jLabel11)
                                .addGap(92, 92, 92)
                                .addComponent(jLabel12)
                                .addGap(82, 82, 82)
                                .addComponent(jLabel13)
                                .addGap(96, 96, 96)
                                .addComponent(jLabel14)
                                .addGap(86, 86, 86)
                                .addComponent(jLabel15)
                                .addGap(87, 87, 87)
                                .addComponent(jLabel16)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pnlTimeBlack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlTimeWhite, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane2)
                            .addComponent(jScrollPane1)
                            .addComponent(btnSendMessage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lblInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addComponent(jLabel1)
                        .addGap(62, 62, 62)
                        .addComponent(jLabel2)
                        .addGap(60, 60, 60)
                        .addComponent(jLabel3)
                        .addGap(60, 60, 60)
                        .addComponent(jLabel4)
                        .addGap(61, 61, 61)
                        .addComponent(jLabel5)
                        .addGap(65, 65, 65)
                        .addComponent(jLabel6)
                        .addGap(60, 60, 60)
                        .addComponent(jLabel7)
                        .addGap(57, 57, 57)
                        .addComponent(jLabel8))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13)
                            .addComponent(jLabel14)
                            .addComponent(jLabel15)
                            .addComponent(jLabel16))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(pnlTimeBlack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnSendMessage)
                                .addGap(9, 9, 9)
                                .addComponent(pnlTimeWhite, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(pnlBoard, javax.swing.GroupLayout.PREFERRED_SIZE, 667, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(43, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void miMultiPlrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miMultiPlrActionPerformed
        isMultiPlayer = true;
        NewGameForm form = new NewGameForm(this);

    }//GEN-LAST:event_miMultiPlrActionPerformed

    private void miDemoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miDemoActionPerformed
        player = new Player(("White"), PieceColor.White);
        game = new ChessGame(player, this);

        startGame(true);
        btnNewGame.setEnabled(false);
    }//GEN-LAST:event_miDemoActionPerformed

    private void btnSurrenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSurrenderActionPerformed
        //TODO: recognize which player pressed surrender
        stopGame();
    }//GEN-LAST:event_btnSurrenderActionPerformed

    private void btnSaveGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveGameActionPerformed

        GameManager.saveCurrentGame(game);
        stopGame();

        new JOptionPane("Game sucessfully saved!", JOptionPane.INFORMATION_MESSAGE).setVisible(true);
    }//GEN-LAST:event_btnSaveGameActionPerformed

    private void btnLoadGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoadGameActionPerformed

        game = GameManager.loadGame(this);
        writeMessage(game.lblMessage);
        startGame(false);
        btnNewGame.setEnabled(false);

    }//GEN-LAST:event_btnLoadGameActionPerformed

    private void btnSendMessageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendMessageActionPerformed

        String message = txtChatInput.getText();

        if (message.isEmpty()) {
            return;
        }

        if (chatManager.sendMessage(message, opponentAddress)) {

            txtChatInput.setText("");
            txtChatInput.setFocusable(true);
            writeChatMessage(message, "ME");
        }

    }//GEN-LAST:event_btnSendMessageActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ChessBoardGUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ChessBoardGUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ChessBoardGUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ChessBoardGUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ChessBoardGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem btnLoadGame;
    private javax.swing.JMenu btnNewGame;
    private javax.swing.JMenu btnOptions;
    private javax.swing.JMenuItem btnSaveGame;
    private javax.swing.JButton btnSendMessage;
    private javax.swing.JMenuItem btnSurrender;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblInfo;
    private javax.swing.JLabel lblNickBlack;
    private javax.swing.JLabel lblNickWhite;
    private javax.swing.JLabel lblTimeBlack;
    private javax.swing.JLabel lblTimeWhite;
    private javax.swing.JMenuItem miDemo;
    private javax.swing.JMenuItem miMultiPlr;
    private javax.swing.JPanel pnlBoard;
    private javax.swing.JPanel pnlTimeBlack;
    private javax.swing.JPanel pnlTimeWhite;
    private javax.swing.JPanel square00;
    private javax.swing.JPanel square01;
    private javax.swing.JPanel square02;
    private javax.swing.JPanel square03;
    private javax.swing.JPanel square04;
    private javax.swing.JPanel square05;
    private javax.swing.JPanel square06;
    private javax.swing.JPanel square07;
    private javax.swing.JPanel square10;
    private javax.swing.JPanel square11;
    private javax.swing.JPanel square12;
    private javax.swing.JPanel square13;
    private javax.swing.JPanel square14;
    private javax.swing.JPanel square15;
    private javax.swing.JPanel square16;
    private javax.swing.JPanel square17;
    private javax.swing.JPanel square20;
    private javax.swing.JPanel square21;
    private javax.swing.JPanel square22;
    private javax.swing.JPanel square23;
    private javax.swing.JPanel square24;
    private javax.swing.JPanel square25;
    private javax.swing.JPanel square26;
    private javax.swing.JPanel square27;
    private javax.swing.JPanel square30;
    private javax.swing.JPanel square31;
    private javax.swing.JPanel square32;
    private javax.swing.JPanel square33;
    private javax.swing.JPanel square34;
    private javax.swing.JPanel square35;
    private javax.swing.JPanel square36;
    private javax.swing.JPanel square37;
    private javax.swing.JPanel square40;
    private javax.swing.JPanel square41;
    private javax.swing.JPanel square42;
    private javax.swing.JPanel square43;
    private javax.swing.JPanel square44;
    private javax.swing.JPanel square45;
    private javax.swing.JPanel square46;
    private javax.swing.JPanel square47;
    private javax.swing.JPanel square50;
    private javax.swing.JPanel square51;
    private javax.swing.JPanel square52;
    private javax.swing.JPanel square53;
    private javax.swing.JPanel square54;
    private javax.swing.JPanel square55;
    private javax.swing.JPanel square56;
    private javax.swing.JPanel square57;
    private javax.swing.JPanel square60;
    private javax.swing.JPanel square61;
    private javax.swing.JPanel square62;
    private javax.swing.JPanel square63;
    private javax.swing.JPanel square64;
    private javax.swing.JPanel square65;
    private javax.swing.JPanel square66;
    private javax.swing.JPanel square67;
    private javax.swing.JPanel square70;
    private javax.swing.JPanel square71;
    private javax.swing.JPanel square72;
    private javax.swing.JPanel square73;
    private javax.swing.JPanel square74;
    private javax.swing.JPanel square75;
    private javax.swing.JPanel square76;
    private javax.swing.JPanel square77;
    private javax.swing.JTextArea txtChatInput;
    private javax.swing.JTextArea txtChatView;
    // End of variables declaration//GEN-END:variables

    private HashMap<String, JPanel> loadPanels() {
        panels = new HashMap<>();

        panels.put("square00", square00);
        panels.put("square10", square10);
        panels.put("square20", square20);
        panels.put("square30", square30);
        panels.put("square40", square40);
        panels.put("square50", square50);
        panels.put("square60", square60);
        panels.put("square70", square70);
        panels.put("square01", square01);
        panels.put("square11", square11);
        panels.put("square21", square21);
        panels.put("square31", square31);
        panels.put("square41", square41);
        panels.put("square51", square51);
        panels.put("square61", square61);
        panels.put("square71", square71);
        panels.put("square02", square02);
        panels.put("square12", square12);
        panels.put("square22", square22);
        panels.put("square32", square32);
        panels.put("square42", square42);
        panels.put("square52", square52);
        panels.put("square62", square62);
        panels.put("square72", square72);
        panels.put("square03", square03);
        panels.put("square13", square13);
        panels.put("square23", square23);
        panels.put("square33", square33);
        panels.put("square43", square43);
        panels.put("square53", square53);
        panels.put("square63", square63);
        panels.put("square73", square73);
        panels.put("square04", square04);
        panels.put("square14", square14);
        panels.put("square24", square24);
        panels.put("square34", square34);
        panels.put("square44", square44);
        panels.put("square54", square54);
        panels.put("square64", square64);
        panels.put("square74", square74);
        panels.put("square05", square05);
        panels.put("square15", square15);
        panels.put("square25", square25);
        panels.put("square35", square35);
        panels.put("square45", square45);
        panels.put("square55", square55);
        panels.put("square65", square65);
        panels.put("square75", square75);
        panels.put("square06", square06);
        panels.put("square16", square16);
        panels.put("square26", square26);
        panels.put("square36", square36);
        panels.put("square46", square46);
        panels.put("square56", square56);
        panels.put("square66", square66);
        panels.put("square76", square76);
        panels.put("square07", square07);
        panels.put("square17", square17);
        panels.put("square27", square27);
        panels.put("square37", square37);
        panels.put("square47", square47);
        panels.put("square57", square57);
        panels.put("square67", square67);
        panels.put("square77", square77);

        return panels;
    }

    private void createMouseEventHandler() {
        adapter = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (!player.getPieceColor().equals(game.getPlayerTurn()) && isMultiPlayer) {
                    return;
                }
                JPanel panel = (JPanel) e.getSource();
                Square s = panelManager.getSquare(panel);
                Piece piece = game.getBoard().getPiece(s);

                unmarkAvailablePanels();
                //Odabrao svoju figuru
                if (piece != null && piece.getColor() == player.getPieceColor()) {
                    selectedPiece = piece;
                    markAvailablePanels(piece);
                } //Odabrao protivničku figuru
                else if (piece != null && piece.getColor() != player.getPieceColor()) {
                    if (selectedPiece != null) {
                        makeMove(s);
                    }//Odabrao prazno polje
                } else if (selectedPiece != null) {
                    makeMove(s);
                }
            }
        };
    }

    private void initTimers(boolean isNewGame) {

        if (isNewGame) {
            resetTimers();
        }
        lblTimeWhite.setText(String.format("%s : %s", game.whiteMinutes, game.whiteSeconds < 10 ? "0" + String.valueOf(game.whiteSeconds) : String.valueOf(game.whiteSeconds)));
        lblTimeBlack.setText(String.format("%s : %s", game.blackMinutes, game.blackSeconds < 10 ? "0" + String.valueOf(game.blackSeconds) : String.valueOf(game.blackSeconds)));

        gameTimer = new Thread(new Runnable() {
            @Override
            public void run() {
                while (game.getResult() == Result.None && !gameTimer.isInterrupted()) {
                    if (game.getPlayerTurn() == PieceColor.White) {
                        game.whiteSeconds--;
                        try {
                            Thread.sleep(1000);

                        } catch (InterruptedException ex) {
                            Logger.getLogger(ChessBoardGUI.class
                                    .getName()).log(Level.SEVERE, null, ex);
                        }
                        lblTimeWhite.setText(String.format("%s : %s", game.whiteMinutes, game.whiteSeconds < 10 ? "0" + String.valueOf(game.whiteSeconds) : String.valueOf(game.whiteSeconds)));
                        if (game.whiteSeconds == 0) {
                            game.whiteSeconds = 60;
                            game.whiteMinutes--;
                            if (game.whiteMinutes < 0) {
                                game.raiseOnStateChanged(PieceColor.White, CheckState.Timeout);
                            }
                        }
                    } else {
                        game.blackSeconds--;
                        try {
                            Thread.sleep(1000);

                        } catch (InterruptedException ex) {
                            Logger.getLogger(ChessBoardGUI.class
                                    .getName()).log(Level.SEVERE, null, ex);
                        }
                        lblTimeBlack.setText(String.format("%s : %s", game.blackMinutes, game.blackSeconds < 10 ? "0" + String.valueOf(game.blackSeconds) : String.valueOf(game.blackSeconds)));
                        if (game.blackSeconds == 0) {
                            game.blackSeconds = 60;
                            game.blackMinutes--;
                            if (game.blackMinutes < 0) {
                                game.raiseOnStateChanged(PieceColor.Black, CheckState.Timeout);
                            }
                        }
                    }
                }
            }
        }
        );

        gameTimer.setDaemon(true);
    }

    private void writeChatMessage(String message, String nickName) {
        txtChatView.setFont(new Font("Segoe UI", Font.BOLD, 18));
        if (!nickName.equals("ME")) {
            txtChatView.setDisabledTextColor(Color.green);
        }
        txtChatView.append(nickName + ": ");
        txtChatView.setDisabledTextColor(Color.black);

        txtChatView.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        txtChatView.append(message + "\n");

    }

}
