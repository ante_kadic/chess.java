/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Events;

import BLL.Square;

/**
 *
 * @author ante.kadic
 */
public class MovementEventArgs {
    public Square from;
    public Square to;

    public MovementEventArgs(Square from, Square to) {
        this.from = from;
        this.to = to;
    }
    
}
