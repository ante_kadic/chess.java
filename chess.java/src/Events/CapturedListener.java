/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Events;

import Enums.CheckState;
import Enums.PieceColor;
import java.util.EventListener;

/**
 *
 * @author ante.kadic
 */
public interface CapturedListener extends EventListener {
    
    void onCapturedEvent(CapturedEvent ev);
    
    void onCheckStateChangedEvent(PieceColor playerOnCheckState,CheckState state);
        
}
