/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Chat;

import UI.ChessBoardGUI;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ExportException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ante.kadic
 */
public class ChatManager {

    private ChatServer obj;
    private ChatServer objServer;
    private ChatClient client;
    private String name;
    Registry registry;
    private int sendingPort = 20200;
    private int listeningPort = 10200;
    private String exportedName = "Chat";
    private String registeringName = "ChatTemp";
    public static String NICKNAME_MESSAGE_FORMAT = "...NiCkNaMeOfOpPonNeNt;;..;;..1";
    public static String PIECECOLOR_MESSAGE_FORMAT = "...PiEcEcLrOfOpPonNeNt;;..;;..2";

    public ChatManager(String name, ChessBoardGUI listener) {
        this.name = name;
        initServer(listener);
    }

    public boolean sendMessage(String message, String host) {

        try {
            Registry registry = LocateRegistry.getRegistry(host, sendingPort);
            ChatServerInterface stub = (ChatServerInterface) registry.lookup(exportedName);
            stub.sendMessage(message, name);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void initServer(ChessBoardGUI listener) {

        try {
            client = new ChatClient(listener);
            obj = new ChatServer(client);
            ChatServerInterface stub = (ChatServerInterface) UnicastRemoteObject.exportObject(obj, 0);

            Registry registry = LocateRegistry.createRegistry(listeningPort);
            registry.bind(registeringName, stub);

        } catch (java.rmi.server.ExportException e) {
            ChatServerInterface stub;
            try {
                client = new ChatClient(listener);
                objServer = new ChatServer(client);
                stub = (ChatServerInterface) UnicastRemoteObject.exportObject(objServer, 0);
                try {
                    registry = LocateRegistry.createRegistry(sendingPort);
                } catch (ExportException ee) {
                    registry = LocateRegistry.getRegistry(sendingPort);
                    registry.unbind(exportedName);
                }
                sendingPort = listeningPort;
                registry.bind(exportedName, stub);
                exportedName = registeringName;
            } catch (Exception ex) {
                Logger.getLogger(ChatManager.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }

    }
}
