/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Chat;

import Events.CapturedListener;
import Events.ChatListener;
import UI.ChessBoardGUI;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author ante.kadic
 */
public class ChatClient implements ChatClientInterface {

    private List<ChatListener> recievedMessageListeners = new LinkedList<ChatListener>();
    
    public ChatClient( ChessBoardGUI listener) throws RemoteException {
        recievedMessageListeners.add(listener);
    }
    
    @Override
    public void retrieveMessage(String msg, String nickName) throws RemoteException {
        for (ChatListener recievedMessageListener : recievedMessageListeners) {
            recievedMessageListener.onRecievedMessage(msg, nickName);
        }
    }

    
}
