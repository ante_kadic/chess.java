/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helpers;

import BLL.ChessBoard;
import BLL.ChessGame;
import PieceModel.Piece;
import UI.ChessBoardGUI;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ante.kadic
 */
public class GameManager {

    private static Socket socket = null;
    private static ServerSocket server = null;
    private static ObjectInputStream input;
    private static ObjectOutputStream output;
    private static int port = 10101;
    private static ChessGame loadedGame;
    private final static String FILE_PATH = "Saved\\saveGame.ser";

    public static void saveCurrentGame(ChessGame game) {

        try {
            FileOutputStream fileOut
                    = new FileOutputStream(FILE_PATH);
            output = new ObjectOutputStream(fileOut);
            output.writeObject(game);
            output.close();
            fileOut.close();

        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public static ChessGame loadGame(ChessBoardGUI guiEventListener) {

        try {
            FileInputStream fileIn = new FileInputStream(FILE_PATH);
            input = new ObjectInputStream(fileIn);
            loadedGame = (ChessGame) input.readObject();
            configureLoadedGame(guiEventListener);

            input.close();
            fileIn.close();
            return loadedGame;
        } catch (IOException i) {
            i.printStackTrace();
            return null;
        } catch (ClassNotFoundException c) {
            System.out.println("Employee class not found");
            c.printStackTrace();
            return null;
        }
    }

    private static void configureLoadedGame(ChessBoardGUI guiEventListener) {
        Piece.setBoard(loadedGame.getBoard());
        loadedGame.addListeners(guiEventListener);
        loadedGame.subscribeForCapturedEvent();

    }

    public static void sendStateToOpponent(ChessGame game) throws IOException {

        output = new ObjectOutputStream(socket.getOutputStream());
        output.writeObject(game);

    }

    public static void connectToOpponent(String address) throws IOException {

        try {
            socket = new Socket(address, port);
        } catch (IOException ex) {
            server = new ServerSocket(port);
            socket = server.accept();
        }
    }

    public static void disconnectFromOpponent() {
        try {
            socket.close();
            if (server != null) {
                server.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(GameManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static ChessGame getStateFromOpponent(ChessBoardGUI guiEventListener) throws IOException, ClassNotFoundException {
        try {
            input = new ObjectInputStream(socket.getInputStream());
        } catch (Exception ex) {
            return null;
        }
        loadedGame = (ChessGame) input.readObject();
        configureLoadedGame(guiEventListener);

        return loadedGame;
    }
}
