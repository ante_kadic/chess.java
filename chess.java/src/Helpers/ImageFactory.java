/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helpers;

import Enums.PieceColor;
import PieceModel.Pawn;
import PieceModel.Piece;
import javafx.scene.control.TableView;
import javax.swing.ImageIcon;

/**
 *
 * @author ante.kadic
 */
public class ImageFactory {

    private static String whitePawnPath = "Image\\WhitePawn.png";
    private static String blackPawnPath = "Image\\BlackPawn.png";
    private static String whiteRookPath = "Image\\WhiteRook.png";
    private static String blackRookPath = "Image\\BlackRook.png";
    private static String whiteKnightPath = "Image\\WhiteKnight.png";
    private static String blackKnightPath = "Image\\BlackKnight.png";
    private static String whiteBishopPath = "Image\\WhiteBishop.png";
    private static String blackBishopPath = "Image\\BlackBishop.png";
    private static String whiteKingPath = "Image\\WhiteKing.png";
    private static String blackKingPath = "Image\\BlackKing.png";
    private static String whiteQueenPath = "Image\\WhiteQueen.png";
    private static String blackQueenPath = "Image\\BlackQueen.png";

    public String getPieceImage(Piece piece) {
        String name = piece.getClass().getSimpleName();

        switch (name) {
            case "Pawn":
                if (piece.getColor() == PieceColor.Black) {
                    return blackPawnPath;
                }
                return whitePawnPath;
            case "Rook":
                if (piece.getColor() == PieceColor.Black) {
                    return blackRookPath;
                }
                return whiteRookPath;
            case "Knight":
                if (piece.getColor() == PieceColor.Black) {
                    return blackKnightPath;
                }
                return whiteKnightPath;
            case "Bishop":
                if (piece.getColor() == PieceColor.Black) {
                    return blackBishopPath;
                }
                return whiteBishopPath;
            case "King":
                if (piece.getColor() == PieceColor.Black) {
                    return blackKingPath;
                }
                return whiteKingPath;
            case "Queen":
                if (piece.getColor() == PieceColor.Black) {
                    return blackQueenPath;
                }
                return whiteQueenPath;
            default:
                return whiteBishopPath;
        }
    }
}
