/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helpers;

import BLL.Square;
import java.util.LinkedList;
import java.util.HashMap;
import javax.swing.JPanel;

/**
 *
 * @author ante.kadic
 */
public class SquarePanelMapper {

    private HashMap<String, JPanel> panels;

    public SquarePanelMapper(HashMap<String, JPanel> panels) {
        this.panels = panels;
    }

    public JPanel getPanel(Square square) {
        return panels.get("square" + String.valueOf(square.getX()) + String.valueOf(square.getY()));
    }

    public Square getSquare(JPanel panel) {
        for (String panelName : panels.keySet()) {
            if (panels.get(panelName) == panel) {
                return new Square(
                        Integer.parseInt(panelName.substring(6, 7)), Integer.parseInt(panelName.substring(7, 8)));
            }
        }
        return null;
    }

}
