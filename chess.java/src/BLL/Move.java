package BLL;

import PieceModel.Piece;
import java.io.Serializable;

public class Move implements Serializable {

    private Square startSquare;
    private Square destinationSquare;
    private Piece piece;

    public Move(Square startSquare, Square destinationSquare, Piece piece) {
        this.startSquare = startSquare;
        this.destinationSquare = destinationSquare;
        this.piece = piece;
    }

    /**
     * @return the startSquare
     */
    public Square getStartSquare() {
        return startSquare;
    }

    /**
     * @return the destinationSquare
     */
    public Square getDestinationSquare() {
        return destinationSquare;
    }

    /**
     * @return the piece
     */
    public Piece getPiece() {
        return piece;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Move) ) {
            return false;
        }
        Move other = (Move) obj;
        if (other.getPiece().equals(this.getPiece()) && other.getStartSquare().equals(this.getStartSquare()) && other.getDestinationSquare().equals(this.getDestinationSquare())){
            return true;
        }
        return false;
    }

    public void setStartSquare(Square startSquare) {
        this.startSquare = startSquare;
    }

    public void setDestinationSquare(Square destinationSquare) {
        this.destinationSquare = destinationSquare;
    }

    /**
     * @param piece the piece to set
     */
    public void setPiece(Piece piece) {
        this.piece = piece;
    }

}
