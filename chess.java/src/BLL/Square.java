package BLL;

import Enums.PieceColor;
import PieceModel.Piece;
import java.io.Serializable;
import javax.swing.ImageIcon;

public class Square implements Serializable {

    private int x;
    private int y;
    private transient ImageIcon image;
    
    public Square(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean isValid() {
        if (x > 7 || x < 0 || y > 7 || y < 0) {
            return false;
        }
        return true;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    /**
     * @return the image
     */
    public ImageIcon getImage() {
        return image;
    }

    public void setImage(ImageIcon image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object other) {
        Square otherSquare = (Square) other;
        return (this.getX() == otherSquare.getX() && this.getY() == otherSquare.getY());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }

}
