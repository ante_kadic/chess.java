package BLL;

import Enums.CheckState;
import Enums.PieceColor;
import Events.CapturedEvent;
import Events.CapturedListener;
import Helpers.PieceMovementManager;
import PieceModel.Bishop;
import PieceModel.King;
import PieceModel.Knight;
import PieceModel.Pawn;
import PieceModel.Piece;
import PieceModel.Queen;
import PieceModel.Rook;
import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ChessBoard implements Serializable {

    private King whiteKing;
    private King blackKing;
    private List<Piece> whitePieces;
    private List<Piece> blackPieces;
    private Piece temporaryCapturedPiece;

    public ChessBoard() {
        whitePieces = new LinkedList<Piece>();
        blackPieces = new LinkedList<Piece>();
        InitPieces();
    }

    private transient List<CapturedListener> capturedListeners = new LinkedList<CapturedListener>();

    public Piece getPiece(Square square, PieceColor color) {
        if (color == PieceColor.White) {

            for (Piece piece : getWhitePieces()) {
                if (piece.getPosition().equals(square)) {
                    return piece;
                }
            }
        } else {
            for (Piece piece : getBlackPieces()) {
                if (piece.getPosition().equals(square)) {
                    return piece;
                }
            }
        }
        return null;
    }

    public Piece getPiece(Square square) {
        for (Piece piece : getWhitePieces()) {
            if (piece.getPosition().equals(square)) {
                return piece;
            }
        }
        for (Piece piece : getBlackPieces()) {
            if (piece.getPosition().equals(square)) {
                return piece;
            }
        }
        return null;
    }

    public boolean isWhiteOccupied(Square square) {
        for (Piece piece : getWhitePieces()) {
            if (piece.getPosition().equals(square)) {
                return true;
            }
        }
        return false;
    }

    public boolean isBlackOccupied(Square square) {
        for (Piece piece : getBlackPieces()) {
            if (piece.getPosition().equals(square)) {
                return true;
            }
        }
        return false;
    }

    public boolean isEmptySquare(Square square) {
        return !isWhiteOccupied(square) && !isBlackOccupied(square);
    }

    public boolean isAttackedSquare(Square square, PieceColor attackingPiece) {
        if (attackingPiece == PieceColor.White) {
            for (Piece whitePiece : getWhitePieces()) {
                if (whitePiece instanceof Pawn) {
                    if (((Pawn) whitePiece).attackingSquares().contains(square)) {
                        return true;
                    }
                } else if (whitePiece instanceof King) {
                    if (PieceMovementManager.getKingSurroundingSquares((King) whitePiece).contains(square)) {
                        return true;
                    }
                } else {
                    if (whitePiece.validMoves().contains(square)) {
                        return true;
                    }
                }
            }
        } else {
            for (Piece blackPiece : getBlackPieces()) {
                if (blackPiece instanceof Pawn) {
                    if (((Pawn) blackPiece).attackingSquares().contains(square)) {
                        return true;
                    }
                } else if (blackPiece instanceof King) {
                    if (PieceMovementManager.getKingSurroundingSquares((King) getBlackKing()).contains(square)) {
                        return true;
                    }
                } else {
                    if (blackPiece.validMoves().contains(square)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean isKingOnCheck(PieceColor color) {
        if (color == PieceColor.Black) {
            return isAttackedSquare(getBlackKing().getPosition(), PieceColor.White);
        } else {
            return isAttackedSquare(getWhiteKing().getPosition(), PieceColor.Black);
        }
    }

    private void InitPieces() {
        Piece.setBoard(this);
        setKingQueenPieces();
        setPawnPieces();
        setKnightPieces();
        setRockPieces();
        setBishopPieces();
    }

    private void setKnightPieces() {
        getWhitePieces().add(new Knight(PieceColor.White, new Square(1, 0)));
        getWhitePieces().add(new Knight(PieceColor.White, new Square(6, 0)));

        getBlackPieces().add(new Knight(PieceColor.Black, new Square(1, 7)));
        getBlackPieces().add(new Knight(PieceColor.Black, new Square(6, 7)));
    }

    private void setRockPieces() {
        getWhitePieces().add(new Rook(PieceColor.White, new Square(0, 0)));
        getWhitePieces().add(new Rook(PieceColor.White, new Square(7, 0)));

        getBlackPieces().add(new Rook(PieceColor.Black, new Square(0, 7)));
        getBlackPieces().add(new Rook(PieceColor.Black, new Square(7, 7)));
    }

    private void setBishopPieces() {
        getWhitePieces().add(new Bishop(PieceColor.White, new Square(2, 0)));
        getWhitePieces().add(new Bishop(PieceColor.White, new Square(5, 0)));

        getBlackPieces().add(new Bishop(PieceColor.Black, new Square(2, 7)));
        getBlackPieces().add(new Bishop(PieceColor.Black, new Square(5, 7)));
    }

    private void setPawnPieces() {
        for (int i = 0; i < 8; i++) {
            getWhitePieces().add(new Pawn(PieceColor.White, new Square(i, 1)));
            getBlackPieces().add(new Pawn(PieceColor.Black, new Square(i, 6)));
        }
    }

    private void setKingQueenPieces() {
        setWhiteKing(new King(PieceColor.White, new Square(4, 0)));
        setBlackKing(new King(PieceColor.Black, new Square(4, 7)));

        getWhitePieces().add(getWhiteKing());
        getBlackPieces().add(getBlackKing());
        getWhitePieces().add(new Queen(PieceColor.White, new Square(3, 0)));
        getBlackPieces().add(new Queen(PieceColor.Black, new Square(3, 7)));
    }

    public boolean makeMove(Move move) {
        if (!move.getPiece().isValidMove(move)) {
            return false;
        }
        move.getPiece().setPosition(move.getDestinationSquare());
        PieceColor opositeColor = move.getPiece().getColor() == PieceColor.Black ? PieceColor.White : PieceColor.Black;
        tryDestroyPiece(getPiece(move.getDestinationSquare(), opositeColor));

        if (!checkIfCheckStateChanged(move.getPiece())) {
            move.getPiece().setPosition(move.getStartSquare());
            returnPieceOnBoard();
            return false;
        } else {
            if (temporaryCapturedPiece != null) {
                returnPieceOnBoard();
                destroyPiece(getPiece(move.getDestinationSquare(), opositeColor));
            }
        }
        temporaryCapturedPiece = null;

        return true;
    }

    private boolean checkIfCheckStateChanged(Piece piece) {
        //Checking is king of current color was on CHECK-state before this move
        boolean isOnCheck = piece.getColor() == PieceColor.Black ? getBlackKing().getOnCheck() : getWhiteKing().getOnCheck();
        //Checking is king of current color is on CHECK-state after this move
        boolean nowOpenedCheck = piece.getColor() == PieceColor.Black ? isKingOnCheck(PieceColor.Black) : isKingOnCheck(PieceColor.White);

        if (!isOnCheck && nowOpenedCheck) {
            return false;
        } else if (isOnCheck && nowOpenedCheck) {
            return false;
        } else if (isOnCheck && !nowOpenedCheck) {
            if (piece.getColor() == PieceColor.Black && getBlackKing().getOnCheck()) {
                getBlackKing().setOnCheck(false, piece);
                return true;
            } else if (piece.getColor() == PieceColor.White && getWhiteKing().getOnCheck()) {
                getWhiteKing().setOnCheck(false, piece);
                return true;
            }
        }

        PieceColor opositeColor = piece.getColor() == PieceColor.Black ? PieceColor.White : PieceColor.Black;
        //Checking if this piece gave a CHECK on opponent
        for (Square validMove : piece.validMoves()) {
            Piece p = getPiece(validMove, opositeColor);
            if (p != null && p instanceof King) {
                ((King) p).setOnCheck(true, piece);
                return true;
            }
        }

        return true;
    }

    public void addOnCaptureListener(CapturedListener subscriber) {
        if (capturedListeners == null) {
            capturedListeners = new LinkedList<CapturedListener>();
        }
        capturedListeners.add(subscriber);
    }

    public void removeOnCaptureListener(CapturedListener subscriber) {
        capturedListeners.remove(subscriber);
    }

    public void raiseOnCaptureEvent(Piece piece) {
        for (CapturedListener listener : capturedListeners) {
            listener.onCapturedEvent(new CapturedEvent(piece));
        }
    }

    public void raiseOnCheckStateChangedEvent(PieceColor player, CheckState newState) {
        for (CapturedListener listener : capturedListeners) {
            listener.onCheckStateChangedEvent(player, newState);
        }
    }

    private void destroyPiece(Piece piece) {

        boolean moved = piece.getColor() == PieceColor.Black ? getBlackPieces().remove(piece) : getWhitePieces().remove(piece);

        raiseOnCaptureEvent(piece);
    }

    /**
     * @return the whitePieces
     */
    public List<Piece> getWhitePieces() {
        return whitePieces;
    }

    /**
     * @return the blackPieces
     */
    public List<Piece> getBlackPieces() {
        return blackPieces;
    }

    private void tryDestroyPiece(Piece piece) {
        if (piece == null) {
            return;
        }
        temporaryCapturedPiece = piece;
        boolean moved = piece.getColor() == PieceColor.Black ? getBlackPieces().remove(piece) : getWhitePieces().remove(piece);

    }

    private void returnPieceOnBoard() {
        if (temporaryCapturedPiece != null) {
            if (temporaryCapturedPiece.getColor() == PieceColor.Black) {
                getBlackPieces().add(temporaryCapturedPiece);
            } else {
                getWhitePieces().add(temporaryCapturedPiece);
            }
        }
    }

    /**
     * @return the whiteKing
     */
    public King getWhiteKing() {
        return whiteKing;
    }

    public void setWhiteKing(King whiteKing) {
        this.whiteKing = whiteKing;
    }

    /**
     * @return the blackKing
     */
    public King getBlackKing() {
        return blackKing;
    }

    public void setBlackKing(King blackKing) {
        this.blackKing = blackKing;
    }

    public void setWhitePieces(List<Piece> whitePieces) {
        this.whitePieces = whitePieces;
    }

    public void setBlackPieces(List<Piece> blackPieces) {
        this.blackPieces = blackPieces;
    }

}
