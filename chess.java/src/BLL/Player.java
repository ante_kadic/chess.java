package BLL;

import Enums.PieceColor;
import java.io.Serializable;

public class Player implements Serializable {

    private String name;
    private PieceColor pieceColor;

    public Player(String name, PieceColor pieceColor) {
        this.name = name;
        this.pieceColor = pieceColor;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the pieceColor
     */
    public PieceColor getPieceColor() {
        return pieceColor;
    }

    public void setPieceColor(PieceColor pieceColor) {
        this.pieceColor = pieceColor;
    }

    @Override
    public boolean equals(Object obj) {
        return this.getName().equals(((Player) obj).getName()) && this.getPieceColor().equals(((Player) obj).getPieceColor());
    }

}
