/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PieceModel;

import Helpers.PieceMovementManager;
import Enums.PieceColor;
import BLL.Square;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author ante.kadic
 */
public class Rook extends Piece implements Serializable {

    public Rook(PieceColor Color, Square initPosition) {
        super(Color, initPosition);
    }

    @Override
    public List<Square> validMoves() {
        return PieceMovementManager.getValidAntiDiagonals(getPosition(), this);
    }

}
